﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ZShared.Kernel;
using ZShared.RobotHelpers;
using ZShared.Helpers;
using RobotOM;
using MathNet.Spatial.Euclidean;
using MathNet.Numerics.LinearAlgebra;
using ClipperLib;


namespace RoofWindLoad
{
    public class RoofWindLoad : ZShared.Kernel.IPlugin
    {
        private UI _ui = null;
        long scaleFactor = 10000000;

        public System.Windows.Forms.Form MainInterface
        {
            get
            {
                if (_ui == null)
                {
                    _ui = new UI(this);
                }
                if (_ui.IsDisposed)
                    _ui = new UI(this);

                return _ui;
            }
        }

        public string MenuName
        {
            get
            {
                return "Obciążenie wiatrem dachu EC3";
            }
        }

        public void Generate()
        {
            IRobotCase currentCase = CaseHelpers.GetCurrentRobotCase();
            IRobotSelection panels = _ui.Panels;
            Dictionary<Areas, double> Forces = new Dictionary<Areas, double>()
            {
                {Areas.F1, _ui.fForce},
                {Areas.F2, _ui.fForce },
                {Areas.G, _ui.gForce },
                {Areas.H, _ui.hForce },
                {Areas.I, _ui.iForce }
            };

            double lh = _ui.Lh;
            double lf = _ui.Lf;
            double bf = _ui.Bf;

            Point3D p1 = _ui.P1;
            print("P1: {0}", p1);
            Point3D p2 = _ui.P2;
            print("P2: {0}", p2);
            Point3D p3 = _ui.P3;
            print("P3: {0}", p3);
            Point3D p4 = _ui.P4;
            print("P4: {0}", p4);

            #region nowe
            List<Point3D> uPoints = new List<Point3D>() { p1, p2, p3, p4 };
            List<Point3D> z0Point = new List<Point3D>();
            //zrzucam na plaszczyzne "0" zeby byc pewnym ze punkty leza na jednej plaszczyznie
            foreach (Point3D p in uPoints)
                z0Point.Add(new Point3D(p.X, p.Y, 0));

            LoadGeometry LG = new LoadGeometry(z0Point);
            LG.Bf = bf;
            LG.Lf = lf;
            LG.Lh = lh;
            #endregion

            #region stare
            //Line3D l1 = new Line3D(p1, p4);
            //print("L1: {0}", l1);
            //print("L1 Direction: {0}", l1.Direction);

            //Plane p0 = new Plane(new UnitVector3D(0, 0, 1), new Point3D(0, 0, 0));
            //Line3D l1p = l1.ProjectOn(p0); //Zrzutowanie linii na płaszczyznę poziomą w razie gdyby były na innych rzędnych 
            //print("l1p: {0}", l1p);
            //print("l1p direction: {0}", l1p.Direction);
            //print("l1p direction perp: {0}", l1p.Direction.Orthogonal);
            //Point3D p1p = p1.ProjectOn(p0);
            //Point3D p2p = p1.ProjectOn(p0);
            //Point3D p3p = p1.ProjectOn(p0);
            //Point3D p4p = p1.ProjectOn(p0);


            //Vector3D mV1 = l1p.Direction.Orthogonal.ScaleBy(-1 * lf);
            //print("mV1: {0} L: {1}", mV1, mV1.Length);

            //Vector3D mV2 = l1p.Direction.ScaleBy(bf);
            //print("mV2: {0} L:{1}", mV2, mV2.Length);

            //Point3D p5p = (p1p.ToVector3D() + mV1).ToPoint3D();
            //Point3D p6p = (p1p.ToVector3D() + mV2 + mV1).ToPoint3D();
            //Point3D p7p = (p1p.ToVector3D() + mV2).ToPoint3D();
            //List<Point3D> fLoadPoints = new List<Point3D>();
            //fLoadPoints.Add(p1p);
            //fLoadPoints.Add(p5p);
            //fLoadPoints.Add(p6p);
            //fLoadPoints.Add(p7p);
            ////fLoadPoints - sa na plaszczyznie poziomej
            //Plane loadPointPlane = new Plane(p1p, p5p, p7p);
            #endregion
            foreach (Areas area in Enum.GetValues(typeof(Areas)))
            {
                List<Point3D> fLoadPoints = LG.GetPointsInGCS(area);
                double force = Forces[area];

                for (int i = 1; i <= panels.Count; i++)
                {
                    int claddingNo = panels.Get(i);
                    RobotObjObject cladding = RobotHelper.RStructure.Objects.Get(claddingNo) as RobotObjObject;
                    IRobotDataObject clad2 = RobotHelper.RStructure.Objects.Get(claddingNo);

                    if (cladding == null)
                    {
                        PluginError(this, new PluginErrorEventArgs(string.Format("Nie mogę odnaleźć panela o numerze: {0}", claddingNo)));
                        return;
                    }
                    List<Point3D> pts = PointsFromPanel(cladding);
                    //tutaj jest błąd, branie trzech punktów niezweryfikowane o punkty współliniowe - w związku z tym plaszczyzna moze zostać
                    // zle zdefiniowana i prowadzic do bledu
                    //Plane panelPlane = new Plane(pts[0], pts[1], pts[2]);
                    Plane panelPlane = RobotHelper.PlaneFromCladding(cladding);

                    List<Point3D> fLoadPointPrim = new List<Point3D>();
                    foreach (Point3D p in fLoadPoints)
                    {
                        Ray3D r = new Ray3D(p, new UnitVector3D(0, 0, 1));
                        fLoadPointPrim.Add(panelPlane.IntersectionWith(r));
                    }
                    List<List<Point3D>> fLoadPaths = Intersect(fLoadPointPrim, pts);
                    foreach (List<Point3D> loadPath in fLoadPaths)
                    {
                        GenerateLoad(loadPath, cladding, LoadOnPlane(force, pts), currentCase);
                    }
                }
            }
        }

        //przeksztalca obciazenei z ukladu lokalnego do globalnego
        double[] LoadOnPlane(double zValue, List<Point3D> cladPts)
        {
            Transformation tr = new Transformation(
                cladPts[0].ToVector().ToArray(), 
                cladPts[2].ToVector().ToArray(), 
                cladPts[3].ToVector().ToArray()
                );

            double[] loadP1 = new double[3];
            double[] loadP2 = new double[3];
            double[] result = new double[3];

            loadP2[0] = 0;
            loadP2[1] = 0;
            loadP2[2] = -zValue;

            double[] lP2 = tr.UnTransform(loadP2);
            double[] lP1 = tr.UnTransform(loadP1);
            for (int i = 0; i < 3; i++)
                result[i] = lP2[i] - lP1[i];

            return result;
        }

        List<IntPoint> ToIntPoint(List<Point3D> pts)
        {
            List<IntPoint> path = new List<IntPoint>();
            foreach (Point3D p in pts)
            {
                path.Add(new IntPoint(p.X * scaleFactor, p.Y * scaleFactor));
            }
            return path;
        }

        List<Point3D> ToPoints3D(List<IntPoint> pts)
        {
            List<Point3D> pt = new List<Point3D>();
            foreach (IntPoint p in pts)
            {
                pt.Add(new Point3D((double)p.X / (double)scaleFactor, (double)p.Y / (double)scaleFactor, 0));
            }
            return pt;
        }

        // l1 i l2 musza byc w tej samej plaszczyznie
        List<List<Point3D>> Intersect(List<Point3D> l1, List<Point3D> l2)
        {
            if (l1.Count < 3)
            {
                PluginError(this, new PluginErrorEventArgs("Ilosc punktow nie pozwala na znalezienie plaszczyzny"));
                throw new InvalidOperationException("Ilosc punktow nie pozwala na znalezienie plaszczyzny");
            }
            if (l2.Count < 3)
            {
                PluginError(this, new PluginErrorEventArgs("Ilosc punktow nie pozwala na znalezienie plaszczyzny"));
                throw new InvalidOperationException("Ilosc punktow nie pozwala na znalezienie plaszczyzny");
            }

            // sprowadzenie do problemu plaskiego - znalezienie trzech niewspolliniowych punktow
            // zeby dobrze wyznaczono macierz transformacji
            // tak naprawdę w macierzy transof
            List<Point3D> nonColinearL2 = GeometryHelpers.Find3NonColinearPoints(l2);

            if (nonColinearL2 == null)
            {
                PluginError(this, new PluginErrorEventArgs("Wszystkie punkty są współplaszczyznowe"));
                throw new InvalidOperationException("Wszystkie punkty są współplaszczyznowe");
            }


            Transformation tr = new Transformation(
                nonColinearL2[0].ToVector().ToArray(), 
                nonColinearL2[1].ToVector().ToArray(), 
                nonColinearL2[2].ToVector().ToArray()
                );

            List<Point3D> l1prim = new List<Point3D>();
            List<Point3D> l2prim = new List<Point3D>();
            foreach(Point3D p in l1)
            {
                double[] pprim = tr.TransformPoint(p.ToVector().ToArray());
                l1prim.Add(new Point3D(pprim));
            }
            foreach(Point3D p in l2)
            {
                double[] pprim = tr.TransformPoint(p.ToVector().ToArray());
                l2prim.Add(new Point3D(pprim));
            }
            //tutaj powinny być już płaskie układy
            foreach (Point3D p in l1prim)
            {
                if (!FuzzyCompare(p.Z, 0))
                    throw new InvalidOperationException(string.Format("Problem powinien być plaski wspolrzedne punktu: {0}", p));
            }
            foreach (Point3D p in l2prim)
            {
                if (!FuzzyCompare(p.Z, 0))
                    throw new InvalidOperationException(string.Format("Problem powinien być plaski wspolrzedne punktu: {0}", p));
            }
            //Teraz zaprzegamy do uciecia clipper'a
            Clipper clipper = new Clipper();
            List<IntPoint> subject = ToIntPoint(l1prim);
            List<IntPoint> clip = ToIntPoint(l2prim);

            clipper.AddPath(subject, PolyType.ptSubject, true);
            clipper.AddPath(clip, PolyType.ptClip, true);

            List<List<IntPoint>> resultPrim = new List<List<IntPoint>>();
            clipper.Execute(ClipType.ctIntersection, resultPrim);
            List<List<Point3D>> resultPrimPoints = new List<List<Point3D>>();
            foreach (List<IntPoint> path in resultPrim)
            {
                resultPrimPoints.Add(ToPoints3D(path));
            }
            List<List<Point3D>> Result = new List<List<Point3D>>();
            foreach (List<Point3D> path in resultPrimPoints)
            {
                List<Point3D> resPath = new List<Point3D>();
                foreach (Point3D point in path)
                {
                    resPath.Add(new Point3D(tr.UnTransform(point.ToVector().ToArray())));
                }
                Result.Add(resPath);
            }
            return Result;
        }

        public static bool FuzzyCompare(double u, double v, double delta = 0.000001)
        {
            return Math.Abs(u - v) < delta;
        }

        private List<Point3D> PointsFromPanel(RobotObjObject obj)
        {
            List<Point3D> pts = new List<Point3D>();
            IRobotCollection col = obj.Main.DefPoints;
            for (int i = 1; i <= col.Count; i++)
            {
                RobotGeoPoint3D pt = col.Get(i);
                pts.Add(new Point3D(pt.X, pt.Y, pt.Z));
            }
            return pts;
        }

        private void GenerateLoad(List<Point3D> pts, RobotObjObject cladding, double[] val, IRobotCase lc)
        {
            IRobotSelection sel = RobotHelper.GetSelection(IRobotObjectType.I_OT_OBJECT);
            sel.AddOne(cladding.Number);
            GenerateLoad(pts, sel, val, lc);
        }

        private void GenerateLoad(List<Point3D> pts, IRobotSelection sel, double[] value, IRobotCase lc)
        {
            if (lc.Type != IRobotCaseType.I_CT_SIMPLE)
            {
                PluginError(this, new PluginErrorEventArgs("Wybrany przypadek musi być przypadkiem prostym!"));
            }
            IRobotSimpleCase slc = lc as IRobotSimpleCase;
            RobotLoadRecordMngr mngr = slc.Records;
            RobotLoadRecordInContour rec = mngr.Create(IRobotLoadRecordType.I_LRT_IN_CONTOUR) as RobotLoadRecordInContour;
            rec.SetValue((short)IRobotInContourRecordValues.I_ICRV_LOCAL, 0);
            rec.SetValue((short)IRobotInContourRecordValues.I_ICRV_NPOINTS, pts.Count);
            rec.SetValue((short)IRobotInContourRecordValues.I_ICRV_PX1, value[0] * 1000);
            rec.SetValue((short)IRobotInContourRecordValues.I_ICRV_PY1, value[1] * 1000);
            rec.SetValue((short)IRobotInContourRecordValues.I_ICRV_PZ1, value[2] * 1000);
            rec.SetValue((short)IRobotInContourRecordValues.I_ICRV_AUTO_DETECT_OBJECTS, 0);
            rec.Objects.Add(sel as RobotSelection);
            for (int i = 1; i < pts.Count + 1; i++)
            {
                rec.SetContourPoint(i, pts[i - 1].X, pts[i - 1].Y, pts[i - 1].Z);
            }
            RobotHelper.RApp.Project.ViewMngr.Refresh();
        }



        public static CoordinateSystem BuildSystem(Point3D point, List<Point3D> pts)
        {
            UnitVector3D u = (pts[1] - pts[0]).Normalize();
            UnitVector3D w = u.CrossProduct((pts[2] - pts[0]).Normalize());
            UnitVector3D v = w.CrossProduct(u);
            return new CoordinateSystem(point, u, v, w);
        }

        private void print(string format, params object[] o)
        {
            Console.WriteLine(format, o);
        }

        public event PluginError PluginError;
    }
}
