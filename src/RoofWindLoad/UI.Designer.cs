﻿namespace RoofWindLoad
{
    partial class UI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UI));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.PanelSelectionControl = new ZShared.Controls.RobotSelectionControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.GForceTextBox = new ZShared.Controls.ZDoubleTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.FForceTextBox = new ZShared.Controls.ZDoubleTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.HForceTextBox = new ZShared.Controls.ZDoubleTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.IForceTextBox = new ZShared.Controls.ZDoubleTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.BfTextBox = new ZShared.Controls.ZDoubleTextBox();
            this.LfTextBox = new ZShared.Controls.ZDoubleTextBox();
            this.LhTextBox = new ZShared.Controls.ZDoubleTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.P4Control = new ZShared.Controls.ZPointControl();
            this.P3Control = new ZShared.Controls.ZPointControl();
            this.P2Control = new ZShared.Controls.ZPointControl();
            this.P1Control = new ZShared.Controls.ZPointControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.GenerateButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(364, 52);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Obiekty do obciążenia";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.PanelSelectionControl, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.84615F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 76.15385F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(358, 33);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // PanelSelectionControl
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.PanelSelectionControl, 2);
            this.PanelSelectionControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelSelectionControl.Location = new System.Drawing.Point(3, 3);
            this.PanelSelectionControl.MaximumSize = new System.Drawing.Size(9999, 26);
            this.PanelSelectionControl.MinimumSize = new System.Drawing.Size(100, 26);
            this.PanelSelectionControl.Name = "PanelSelectionControl";
            this.PanelSelectionControl.Selection = "";
            this.PanelSelectionControl.Size = new System.Drawing.Size(352, 26);
            this.PanelSelectionControl.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Location = new System.Drawing.Point(3, 61);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(364, 183);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Wartości obciążeń";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.62924F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.37076F));
            this.tableLayoutPanel2.Controls.Add(this.GForceTextBox, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.FForceTextBox, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.HForceTextBox, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.IForceTextBox, 1, 3);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(358, 164);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // GForceTextBox
            // 
            this.GForceTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GForceTextBox.Location = new System.Drawing.Point(266, 29);
            this.GForceTextBox.Name = "GForceTextBox";
            this.GForceTextBox.Size = new System.Drawing.Size(89, 20);
            this.GForceTextBox.TabIndex = 3;
            this.GForceTextBox.Value = 0D;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(257, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Obciążenie wiatrem strefy G";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FForceTextBox
            // 
            this.FForceTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FForceTextBox.Location = new System.Drawing.Point(266, 3);
            this.FForceTextBox.Name = "FForceTextBox";
            this.FForceTextBox.Size = new System.Drawing.Size(89, 20);
            this.FForceTextBox.TabIndex = 0;
            this.FForceTextBox.Value = 0D;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Obciążenie wiatrem strefy F";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(3, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(257, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "Obciążenie wiatrem strefy H";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HForceTextBox
            // 
            this.HForceTextBox.Location = new System.Drawing.Point(266, 55);
            this.HForceTextBox.Name = "HForceTextBox";
            this.HForceTextBox.Size = new System.Drawing.Size(89, 20);
            this.HForceTextBox.TabIndex = 5;
            this.HForceTextBox.Value = 0D;
            // 
            // label4
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.label4, 2);
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(3, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(286, 59);
            this.label4.TabIndex = 6;
            this.label4.Text = "Uwaga!\r\nZnak \"-\" oznacza ssanie\r\nZnak \"+\" oznacza parcie";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(3, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(257, 26);
            this.label8.TabIndex = 7;
            this.label8.Text = "Obciążenie wiatrem strefy I";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IForceTextBox
            // 
            this.IForceTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IForceTextBox.Location = new System.Drawing.Point(266, 81);
            this.IForceTextBox.Name = "IForceTextBox";
            this.IForceTextBox.Size = new System.Drawing.Size(89, 20);
            this.IForceTextBox.TabIndex = 8;
            this.IForceTextBox.Value = 0D;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.flowLayoutPanel2);
            this.groupBox3.Location = new System.Drawing.Point(3, 250);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(364, 48);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Inne ustawienia";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.checkBox1);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(358, 29);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoCheck = false;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(3, 3);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(350, 24);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Wstawiaj węzły w miejscach charakterystycznych dla obciążenia";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.groupBox1);
            this.flowLayoutPanel1.Controls.Add(this.groupBox2);
            this.flowLayoutPanel1.Controls.Add(this.groupBox3);
            this.flowLayoutPanel1.Controls.Add(this.groupBox4);
            this.flowLayoutPanel1.Controls.Add(this.groupBox6);
            this.flowLayoutPanel1.Controls.Add(this.groupBox5);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(821, 585);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel3);
            this.groupBox4.Location = new System.Drawing.Point(3, 304);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(364, 100);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Wymiary";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.BfTextBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.LfTextBox, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.LhTextBox, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(358, 81);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // BfTextBox
            // 
            this.BfTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BfTextBox.Location = new System.Drawing.Point(182, 3);
            this.BfTextBox.MinimumSize = new System.Drawing.Size(50, 20);
            this.BfTextBox.Name = "BfTextBox";
            this.BfTextBox.Size = new System.Drawing.Size(173, 20);
            this.BfTextBox.TabIndex = 0;
            this.BfTextBox.Value = 0D;
            // 
            // LfTextBox
            // 
            this.LfTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LfTextBox.Location = new System.Drawing.Point(182, 29);
            this.LfTextBox.MinimumSize = new System.Drawing.Size(50, 20);
            this.LfTextBox.Name = "LfTextBox";
            this.LfTextBox.Size = new System.Drawing.Size(173, 20);
            this.LfTextBox.TabIndex = 1;
            this.LfTextBox.Value = 0D;
            // 
            // LhTextBox
            // 
            this.LhTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LhTextBox.Location = new System.Drawing.Point(182, 55);
            this.LhTextBox.MinimumSize = new System.Drawing.Size(50, 20);
            this.LhTextBox.Name = "LhTextBox";
            this.LhTextBox.Size = new System.Drawing.Size(173, 23);
            this.LhTextBox.TabIndex = 2;
            this.LhTextBox.Value = 0D;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(173, 26);
            this.label5.TabIndex = 3;
            this.label5.Text = "Bf [m]";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(3, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 26);
            this.label6.TabIndex = 4;
            this.label6.Text = "Lf [m]";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(3, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(173, 29);
            this.label7.TabIndex = 5;
            this.label7.Text = "Lh [m]";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.P4Control);
            this.groupBox6.Controls.Add(this.P3Control);
            this.groupBox6.Controls.Add(this.P2Control);
            this.groupBox6.Controls.Add(this.P1Control);
            this.groupBox6.Location = new System.Drawing.Point(3, 410);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(364, 150);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Punkty obciążenia";
            // 
            // P4Control
            // 
            this.P4Control.Location = new System.Drawing.Point(6, 115);
            this.P4Control.MaximumSize = new System.Drawing.Size(8000, 25);
            this.P4Control.MinimumSize = new System.Drawing.Size(175, 25);
            this.P4Control.Name = "P4Control";
            this.P4Control.NodeNumber = 0;
            this.P4Control.Size = new System.Drawing.Size(350, 25);
            this.P4Control.TabIndex = 3;
            this.P4Control.X = 0D;
            this.P4Control.Y = 0D;
            this.P4Control.Z = 0D;
            // 
            // P3Control
            // 
            this.P3Control.Location = new System.Drawing.Point(6, 83);
            this.P3Control.MaximumSize = new System.Drawing.Size(8000, 25);
            this.P3Control.MinimumSize = new System.Drawing.Size(175, 25);
            this.P3Control.Name = "P3Control";
            this.P3Control.NodeNumber = 0;
            this.P3Control.Size = new System.Drawing.Size(350, 25);
            this.P3Control.TabIndex = 2;
            this.P3Control.X = 0D;
            this.P3Control.Y = 0D;
            this.P3Control.Z = 0D;
            // 
            // P2Control
            // 
            this.P2Control.Location = new System.Drawing.Point(6, 51);
            this.P2Control.MaximumSize = new System.Drawing.Size(8000, 25);
            this.P2Control.MinimumSize = new System.Drawing.Size(175, 25);
            this.P2Control.Name = "P2Control";
            this.P2Control.NodeNumber = 0;
            this.P2Control.Size = new System.Drawing.Size(350, 25);
            this.P2Control.TabIndex = 1;
            this.P2Control.X = 0D;
            this.P2Control.Y = 0D;
            this.P2Control.Z = 0D;
            // 
            // P1Control
            // 
            this.P1Control.Location = new System.Drawing.Point(6, 19);
            this.P1Control.MaximumSize = new System.Drawing.Size(8000, 25);
            this.P1Control.MinimumSize = new System.Drawing.Size(175, 25);
            this.P1Control.Name = "P1Control";
            this.P1Control.NodeNumber = 0;
            this.P1Control.Size = new System.Drawing.Size(350, 25);
            this.P1Control.TabIndex = 0;
            this.P1Control.X = 0D;
            this.P1Control.Y = 0D;
            this.P1Control.Z = 0D;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.pictureBox1);
            this.groupBox5.Location = new System.Drawing.Point(373, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(434, 388);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ilustracja";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(428, 369);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // GenerateButton
            // 
            this.GenerateButton.Location = new System.Drawing.Point(759, 604);
            this.GenerateButton.Name = "GenerateButton";
            this.GenerateButton.Size = new System.Drawing.Size(75, 23);
            this.GenerateButton.TabIndex = 4;
            this.GenerateButton.Text = "Generuj";
            this.GenerateButton.UseVisualStyleBackColor = true;
            this.GenerateButton.Click += new System.EventHandler(this.GenerateButton_Click);
            // 
            // UI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 633);
            this.Controls.Add(this.GenerateButton);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UI";
            this.Text = "Obciążenie wiatrem dachu wg EC";
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private ZShared.Controls.RobotSelectionControl PanelSelectionControl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private ZShared.Controls.ZDoubleTextBox GForceTextBox;
        private System.Windows.Forms.Label label2;
        private ZShared.Controls.ZDoubleTextBox FForceTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private ZShared.Controls.ZDoubleTextBox HForceTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private ZShared.Controls.ZDoubleTextBox BfTextBox;
        private ZShared.Controls.ZDoubleTextBox LfTextBox;
        private ZShared.Controls.ZDoubleTextBox LhTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox6;
        private ZShared.Controls.ZPointControl P4Control;
        private ZShared.Controls.ZPointControl P3Control;
        private ZShared.Controls.ZPointControl P2Control;
        private ZShared.Controls.ZPointControl P1Control;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button GenerateButton;
        private System.Windows.Forms.Label label8;
        private ZShared.Controls.ZDoubleTextBox IForceTextBox;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}