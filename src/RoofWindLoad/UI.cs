﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RobotOM;
using MathNet.Spatial.Euclidean;
using ZShared;

namespace RoofWindLoad
{
    public partial class UI : Form
    {
        private RoofWindLoad _parent = null;

        public RobotSelection Panels
        {
           get
            {
                return PanelSelectionControl.GetRobotSelection();
            }
            set
            {
                PanelSelectionControl.SetRobotSelection(value);
            }
        }

        public double fForce
        {
            get
            {
                return FForceTextBox.Value;
            }
            set
            {
                FForceTextBox.Value = value;
            }
        }
        public double gForce
        {
            get
            {
                return GForceTextBox.Value;
            }
            set
            {
                GForceTextBox.Value = value;
            }
        }
        public double hForce
        {
            get
            {
                return HForceTextBox.Value;
            }
            set
            {
                HForceTextBox.Value = value;
            }
        }
        public double iForce
        {
            get
            {
                return IForceTextBox.Value;
            }
            set
            {
                IForceTextBox.Value = value;
            }
        }

        public double Lh
        {
            get
            {
                return LhTextBox.Value;
            }
            set
            {
                LhTextBox.Value = value;
            }
        }
        public double Lf
        {
            get
            {
                return LfTextBox.Value;
            }
            set
            {
                LfTextBox.Value = value;
            }
        }
        public double Bf
        {
            get
            {
                return BfTextBox.Value;
            }
            set
            {
                BfTextBox.Value = value;
            }
        }

        public Point3D P1
        {
            get
            {
                return new Point3D(P1Control.GetPointData());
            }
            set
            {
                P1Control.X = value.X;
                P1Control.Y = value.Y;
                P1Control.Z = value.Z;
            }
        }
        public Point3D P2
        {
            get
            {
                return new Point3D(P2Control.GetPointData());
            }
            set
            {
                P2Control.X = value.X;
                P2Control.Y = value.Y;
                P2Control.Z = value.Z;
            }
        }
        public Point3D P3
        {
            get
            {
                return new Point3D(P3Control.GetPointData());
            }
            set
            {
                P3Control.X = value.X;
                P3Control.Y = value.Y;
                P3Control.Z = value.Z;
            }
        }
        public Point3D P4
        {
            get
            {
                return new Point3D(P4Control.GetPointData());
            }
            set
            {
                P4Control.X = value.X;
                P4Control.Y = value.Y;
                P4Control.Z = value.Z;
            }
        }

        public UI(RoofWindLoad parent)
        {
            InitializeComponent();
            _parent = parent;

            PanelSelectionControl.ObjectType = RobotOM.IRobotObjectType.I_OT_OBJECT;
            InitializeTestData();
        }

        private void InitializeTestData()
        {
            PanelSelectionControl.Selection = "37do42";
            FForceTextBox.Value = -2;
            GForceTextBox.Value = -1;
            HForceTextBox.Value = -0.5;
            IForceTextBox.Value = 0.11;

            BfTextBox.Value = 12.0;
            LfTextBox.Value = 2;
            LhTextBox.Value = 16;

            P1Control.X = 0;
            P1Control.Y = 10;
            P1Control.Z = 10;

            P2Control.X = 0;
            P2Control.Y = -50;
            P2Control.Z = 10;

            P3Control.X = 50;
            P3Control.Y = -50;
            P3Control.Z = 10;

            P4Control.X = 50;
            P4Control.Y = 10;
            P4Control.Z = 10;
        }

        private void GenerateButton_Click(object sender, EventArgs e)
        {
            _parent.Generate();
        }
    }
}
