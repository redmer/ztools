﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MathNet.Spatial.Euclidean;
using ZShared.Helpers;

namespace RoofWindLoad
{
    public enum Areas { F1, F2, G, H, I };

    public class LoadGeometry
    {
        List<Point3D> InputPolygon { get; }
        public List<Point3D> TransformedPolygon { get; }

        public double MaxX
        {
            get
            {
                return (from a in TransformedPolygon select a.X).Max();
            }
        }
        public double MinX
        {
            get
            {
                return (from a in TransformedPolygon select a.X).Min();
            }
        }
        public double MaxY
        {
            get
            {
                return (from a in TransformedPolygon select a.Y).Max();
            }
        }
        public double MinY
        {
            get
            {
                return (from a in TransformedPolygon select a.Y).Min();
            }
        }

        Transformation TR;

        public double Lf { get; set; } = -1;
        public double Lh { get; set; } = -1;
        public double Bf { get; set; } = -1;

        private List<Point3D> InternalResults = new List<Point3D>();
        private Dictionary<Areas, List<int>> ResultIndexes = new Dictionary<Areas, List<int>>()
            {
                { Areas.F1, new List<int>() {0, 1, 5, 4 } },
                { Areas.F2, new List<int>() {2, 3, 7, 6 } },
                { Areas.G, new List<int>() {1, 2, 6, 5 } },
                { Areas.H, new List<int>() {4, 7, 9, 8 } },
                { Areas.I, new List<int>() {8, 9, 11, 10 } }
            };

        public LoadGeometry(List<Point3D> MainPolygon)
        {
            InputPolygon = MainPolygon;
            TransformedPolygon = new List<Point3D>();

            TR = new Transformation(
                MainPolygon[0].ToVector().ToArray(),
                MainPolygon[1].ToVector().ToArray(),
                MainPolygon[2].ToVector().ToArray()
                );

            TransformSubject(InputPolygon);
        }

        private void TransformSubject(List<Point3D> p)
        {
            TransformedPolygon.Clear();
            foreach (Point3D pt in p)
                TransformedPolygon.Add(new Point3D(TR.TransformPoint(pt.ToVector().ToArray())));
        }

        private void InternalCalculate()
        {
            List<Point3D> ps = new List<Point3D>();
            Point3D P2 = TransformedPolygon[1];
            Vector3D v1 = new Vector3D(0, Lf, 0);
            Vector3D v2 = new Vector3D(0, Lh, 0);
            Vector3D v3 = new Vector3D(0, MaxY - Lf - Lh, 0);

            ps.Add(new Point3D(MinX, 0, 0)); //Q1           0
            ps.Add(new Point3D(Bf, 0, 0)); //Q2              1
            ps.Add(new Point3D(P2.X - Bf, 0, 0)); //Q3      2
            ps.Add(new Point3D(MaxX, 0, 0)); //Q4           3
            ps.Add(ps[0] + v1);             //Q5            4
            ps.Add(ps[1] + v1);             //Q6            5
            ps.Add(ps[2] + v1);             //Q7            6
            ps.Add(ps[3] + v1);             //Q8            7
            ps.Add(ps[4] + v2);             //Q9            8
            ps.Add(ps[7] + v2);             //Q10           9
            ps.Add(ps[8] + v3);             //Q11           10
            ps.Add(ps[9] + v3);             //Q12           11

            InternalResults = ps;
        }

        public List<Point3D> GetPointsInGCS(Areas Area)
        {
            List<Point3D> results = GetPointsInLCS(Area);
            for (int i = 0; i < results.Count; i++)
            {
                double[] p = TR.UnTransform(results[i].ToVector().ToArray());
                results[i] = new Point3D(p);
            }
            return results;
        }

        public List<Point3D> GetPointsInLCS(Areas Area)
        {
            InternalCalculate();
            List<Point3D> areas = new List<Point3D>();
            foreach (int i in ResultIndexes[Area])
            {
                areas.Add(InternalResults[i]);
            }
            return areas;
        }
    }
}
