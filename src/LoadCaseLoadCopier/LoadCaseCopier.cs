﻿using System;
using System.Collections.Generic;

using RobotOM;
using ZShared.Kernel;
using ZShared.RobotHelpers;

namespace LoadCaseLoadCopier
{
    public class LoadCaseCopier : IPlugin
    {
        private UI _ui = null;

        public System.Windows.Forms.Form MainInterface
        {
            get
            {
                if (_ui == null)
                    _ui = new UI(this);
                if (_ui.IsDisposed)
                    _ui = new UI(this);
                return _ui;
            }
        }

        public string MenuName
        {
            get
            {
                return "Kopiuj obciążenia pomiędzy przypadkami";
            }
        }
        
        public IEnumerable<IRobotSimpleCase> GetLoadCases()
        {
            IRobotCaseServer cases = RobotHelper.RStructure.Cases;
            RobotCaseCollection col = cases.GetAll();
            if (col.Count == 0)
                throw (new InvalidOperationException("Brak przypadków w modelu!"));
            List<IRobotSimpleCase> ret = new List<IRobotSimpleCase>();
            for (int i = 1; i < col.Count + 1; i++)
            {
                IRobotCase curCase = col.Get(i);
                if (curCase.Type == IRobotCaseType.I_CT_SIMPLE)
                    ret.Add(curCase as IRobotSimpleCase);
            }
            return ret;
        }

        public void DoJob()
        {
            IRobotSimpleCase sourceLC = _ui.SourceLC;
            IRobotSimpleCase targetLC = _ui.TargetLC;

            IRobotLoadRecordMngr sMngr = sourceLC.Records;
            IRobotLoadRecordMngr tMngr = targetLC.Records;

            for (int i = 1; i < sMngr.Count + 1; i++)
            {
                IRobotLoadRecord rec = sMngr.Get(i);
                int newLoadNo = tMngr.New(rec.Type);
                IRobotLoadRecord trec = tMngr.Get(newLoadNo);
                switch (rec.Type)
                {
                    case IRobotLoadRecordType.I_LRT_NODE_FORCE:
                        CopyValues(rec, trec);
                        break;
                }
            }
        }

        private void CopyValues(IRobotLoadRecord sourceLoad, IRobotLoadRecord targetLoad)
        {
            for (short i = 0; i < 10; i++)
            {
                double value = sourceLoad.GetValue(i);
                targetLoad.SetValue(i, value);
            }
        }

        public event PluginError PluginError;
    }
}
