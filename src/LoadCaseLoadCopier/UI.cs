﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using RobotOM;

namespace LoadCaseLoadCopier
{
    public partial class UI : Form
    {
        LoadCaseCopier _parent = null;

        public IRobotSimpleCase SourceLC
        {
            get
            {
                CaseListItem i = SourceComboBox.SelectedItem as CaseListItem;
                return i.Case;
            }
            set
            {
                foreach (object obj in SourceComboBox.Items)
                {
                    CaseListItem i = obj as CaseListItem;
                    if (i.Case == value)
                        SourceComboBox.SelectedItem = i;
                }
            }
        }

        public IRobotSimpleCase TargetLC
        {
            get
            {
                CaseListItem i = TargetComboBox.SelectedItem as CaseListItem;
                return i.Case;
            }
            set
            {
                foreach (object obj in TargetComboBox.Items)
                {
                    CaseListItem i = obj as CaseListItem;
                    if (i.Case == value)
                        TargetComboBox.SelectedItem = i;
                }
            }
        }



        public UI(LoadCaseCopier parent)
        {
            InitializeComponent();
            _parent = parent;
            IEnumerable<IRobotSimpleCase> cases = _parent.GetLoadCases();
            SetList(cases, SourceComboBox);
            SetList(cases, TargetComboBox);
        }

        public void SetList(IEnumerable<IRobotSimpleCase> list, ComboBox box)
        {
            foreach (IRobotSimpleCase c in list)
            {
                box.Items.Add(new CaseListItem(c));
                box.SelectedIndex = 0;
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _parent.DoJob();
        }
    }

    public class CaseListItem
    {
        public int Number
        {
            get
            {
                return Case.Number;
            }

        }
        public string Name
        {
            get
            {
                return Case.Name;
            }
        }
        public IRobotSimpleCase Case { get; set; }

        public CaseListItem(IRobotSimpleCase Case)
        {
            this.Case = Case;
        }

        public override string ToString()
        {
            return String.Format("{0} - {1}", Number, Name);
        }
    }
}
