﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ZShared.Kernel;

namespace ZTools
{
    class AvailablePlugin
    {
        public IPlugin Instance { get; set; }
        public string AssemblyPath { get; set; }
    }
}
