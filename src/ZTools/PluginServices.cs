﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using ZShared.Kernel;

namespace ZTools
{
    class PluginServices
    {
        public ICollection<AvailablePlugin> AvailablePlugins { get; set; }

        public PluginServices()
        {
            AvailablePlugins = new List<AvailablePlugin>();
        }

        public void LoadPlugins(string path)
        {
            AvailablePlugins.Clear();
            foreach (string f in Directory.GetFiles(path))
            {
                FileInfo nfo = new FileInfo(f);
                if (nfo.Extension.Equals(".dll"))
                {
                    Assembly exporterAssembly = Assembly.LoadFrom(nfo.FullName);
                    foreach (Type pluginType in exporterAssembly.GetTypes())
                    {
                        if (!pluginType.IsAbstract)
                        {
                            Type typeInterface = pluginType.GetInterface("ZShared.Kernel.IPlugin", true);
                            if (typeInterface != null)
                            {
                                AvailablePlugin exp = new AvailablePlugin();
                                exp.AssemblyPath = path;
                                string pt = pluginType.ToString();
                                Type t = exporterAssembly.GetType(pt);
                                var test = Activator.CreateInstance(t);
                                exp.Instance = test as IPlugin;
                                AvailablePlugins.Add(exp);
                            } 
                        }
                    }
                }
            }
        }
    }
}
