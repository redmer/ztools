﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ZShared.Kernel;

using System.Runtime.InteropServices;

namespace ZTools
{
    [ComVisible(false)]
    public partial class MainWindow : Form
    {
        Settings Settings;

        PluginServices Services = new PluginServices();

        public MainWindow()
        {
            InitializeComponent();
            Settings = Settings.Instance;
            FindPlugins();
            ResizeToButtons();

            checkBox1.Checked = true;
        }

        private void ResizeToButtons()
        {
            int ItemsPerRow = 7;
            var q = (double)Services.AvailablePlugins.Count / (double)ItemsPerRow;
            double dceil = Math.Ceiling(q);
            int NumberOfRows = (int)dceil;
            int width = 584;
            int HeaderHeight = 50;
            int bHeight = ((Size)Settings["MainWindow.Button.Size"]).Height;
            this.Size = new Size(width, HeaderHeight + bHeight * NumberOfRows + (int)tableLayoutPanel1.RowStyles[0].Height);

        }

        private ICollection<AvailablePlugin> FindPlugins()
        {
            try
            {
                string f = Settings["PluginsPath"].ToString();
                Services.LoadPlugins(f);
                foreach (AvailablePlugin p in Services.AvailablePlugins)
                {
                    Button b = new Button();
                    flowLayoutPanel1.Controls.Add(b);
                    b.Size = (Size)Settings["MainWindow.Button.Size"];
                    b.Tag = p.Instance;
                    b.Text = p.Instance.MenuName;
                    b.Click += B_Click;
                }
                return Services.AvailablePlugins;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas odtwarzania wtyczek: {0}", ex.Message, MessageBoxButtons.OK);
                return new List<AvailablePlugin>();
            }
        }

        private void B_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            IPlugin p = b.Tag as IPlugin;
            try
            {
                p.MainInterface.Show();
                this.WindowState = FormWindowState.Minimized;
                p.MainInterface.WindowState = FormWindowState.Normal;
                p.MainInterface.Focus();
                p.MainInterface.FormClosing += MainInterface_FormClosing;
                p.PluginError += P_PluginError;
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format("Wtyczka zwróciła błąd: {0}", ex.Message),
                    "Błąd",
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Warning);
            }
        }

        private void P_PluginError(object sender, PluginErrorEventArgs args)
        {
            MessageBox.Show(args.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        void MainInterface_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form s = sender as Form;
            s.Hide();
            e.Cancel = true;
            WindowState = FormWindowState.Normal;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            TopMost = checkBox1.Checked;
        }
    }
}
