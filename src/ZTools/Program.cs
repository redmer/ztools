﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using RobotOM;

namespace ZTools
{
    [System.Runtime.InteropServices.ComVisible(true), System.Runtime.InteropServices.Guid("9FC77B1E-16EF-453E-840A-E6DA5053D947")]
    public class Program : IRobotAddIn
    {
        private IRobotApplication _iapp = null;

        private enum MenuIds
        {
            ShowMainWindow = 1,
            Settings = 2,

        };

        public bool Connect(RobotApplication robot_app, int add_in_id, bool first_time)
        {
            _iapp = robot_app;
            ZShared.RobotHelpers.RobotHelper.Connect(robot_app);
            return true;
        }

        public bool Disconnect()  
        {
            _iapp = null;
            return true;
        }

        public void DoCommand(int cmd_id)
        {
            MenuIds cmd = (MenuIds)cmd_id;
            switch(cmd)
            {
                case MenuIds.Settings:
                    throw new NotImplementedException();
                case MenuIds.ShowMainWindow:
                    ShowMainWindow();
                    break;
                default:
                    ShowMainWindow();
                    break;
            }
        }

        private void ShowMainWindow()
        {
            MainWindow _mw = new MainWindow();
            _mw.Show();
        }

        public double GetExpectedVersion()
        {
            return 0;
        }

        public int InstallCommands(RobotCmdList cmd_list)
        {
            cmd_list.New((int)MenuIds.ShowMainWindow, "Uruchom...");
            cmd_list.New((int)MenuIds.Settings, "Ustawienia...");
            return cmd_list.Count;
        }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
    }
}
