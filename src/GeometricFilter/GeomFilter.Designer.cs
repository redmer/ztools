﻿namespace GeomFilterNamespace
{
    partial class GeomFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeomFilter));
            this.MainButton = new System.Windows.Forms.Button();
            this.ObjectTabControl = new System.Windows.Forms.TabControl();
            this.BarsPage = new System.Windows.Forms.TabPage();
            this.VectorRadioButton = new System.Windows.Forms.RadioButton();
            this.ZBarsRadioButton = new System.Windows.Forms.RadioButton();
            this.YBarsRadioButton = new System.Windows.Forms.RadioButton();
            this.XBarsRadioButton = new System.Windows.Forms.RadioButton();
            this.YOZBarsRadioButton = new System.Windows.Forms.RadioButton();
            this.XOYBarsRadioButton = new System.Windows.Forms.RadioButton();
            this.XOZBarsRadioButton = new System.Windows.Forms.RadioButton();
            this.PanelsPage = new System.Windows.Forms.TabPage();
            this.ZPanelsRadioButton = new System.Windows.Forms.RadioButton();
            this.YPanelsRadioButton = new System.Windows.Forms.RadioButton();
            this.XPanelsRadioButton = new System.Windows.Forms.RadioButton();
            this.YOZPanelsRadioButton = new System.Windows.Forms.RadioButton();
            this.XOYPanelsRadioButton = new System.Windows.Forms.RadioButton();
            this.XOZPanelsRadioButton = new System.Windows.Forms.RadioButton();
            this.AddCheckBox = new System.Windows.Forms.CheckBox();
            this.LevelLabel = new System.Windows.Forms.Label();
            this.ToleranceTextBox = new System.Windows.Forms.TextBox();
            this.robotSelectionControl1 = new ZShared.Controls.RobotSelectionControl();
            this.ObjectTabControl.SuspendLayout();
            this.BarsPage.SuspendLayout();
            this.PanelsPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainButton
            // 
            this.MainButton.Location = new System.Drawing.Point(99, 286);
            this.MainButton.Margin = new System.Windows.Forms.Padding(2);
            this.MainButton.Name = "MainButton";
            this.MainButton.Size = new System.Drawing.Size(144, 28);
            this.MainButton.TabIndex = 3;
            this.MainButton.Text = "Filtruj";
            this.MainButton.UseVisualStyleBackColor = true;
            this.MainButton.Click += new System.EventHandler(this.MainButton_Click);
            // 
            // ObjectTabControl
            // 
            this.ObjectTabControl.Controls.Add(this.BarsPage);
            this.ObjectTabControl.Controls.Add(this.PanelsPage);
            this.ObjectTabControl.Location = new System.Drawing.Point(5, 13);
            this.ObjectTabControl.Name = "ObjectTabControl";
            this.ObjectTabControl.SelectedIndex = 0;
            this.ObjectTabControl.Size = new System.Drawing.Size(240, 200);
            this.ObjectTabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ObjectTabControl.TabIndex = 4;
            // 
            // BarsPage
            // 
            this.BarsPage.Controls.Add(this.robotSelectionControl1);
            this.BarsPage.Controls.Add(this.VectorRadioButton);
            this.BarsPage.Controls.Add(this.ZBarsRadioButton);
            this.BarsPage.Controls.Add(this.YBarsRadioButton);
            this.BarsPage.Controls.Add(this.XBarsRadioButton);
            this.BarsPage.Controls.Add(this.YOZBarsRadioButton);
            this.BarsPage.Controls.Add(this.XOYBarsRadioButton);
            this.BarsPage.Controls.Add(this.XOZBarsRadioButton);
            this.BarsPage.Location = new System.Drawing.Point(4, 22);
            this.BarsPage.Name = "BarsPage";
            this.BarsPage.Padding = new System.Windows.Forms.Padding(3);
            this.BarsPage.Size = new System.Drawing.Size(232, 174);
            this.BarsPage.TabIndex = 0;
            this.BarsPage.Text = "Pręty";
            this.BarsPage.UseVisualStyleBackColor = true;
            // 
            // VectorRadioButton
            // 
            this.VectorRadioButton.AutoSize = true;
            this.VectorRadioButton.Location = new System.Drawing.Point(15, 99);
            this.VectorRadioButton.Name = "VectorRadioButton";
            this.VectorRadioButton.Size = new System.Drawing.Size(123, 17);
            this.VectorRadioButton.TabIndex = 19;
            this.VectorRadioButton.Text = "Równolegle do pręta";
            this.VectorRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.VectorRadioButton.UseVisualStyleBackColor = true;
            this.VectorRadioButton.CheckedChanged += new System.EventHandler(this.VectorRadioButton_CheckedChanged);
            // 
            // ZBarsRadioButton
            // 
            this.ZBarsRadioButton.AutoSize = true;
            this.ZBarsRadioButton.Location = new System.Drawing.Point(179, 64);
            this.ZBarsRadioButton.Name = "ZBarsRadioButton";
            this.ZBarsRadioButton.Size = new System.Drawing.Size(39, 17);
            this.ZBarsRadioButton.TabIndex = 18;
            this.ZBarsRadioButton.Text = "|| Z";
            this.ZBarsRadioButton.UseVisualStyleBackColor = true;
            this.ZBarsRadioButton.CheckedChanged += new System.EventHandler(this.ZBarsRadioButton_CheckedChanged);
            // 
            // YBarsRadioButton
            // 
            this.YBarsRadioButton.AutoSize = true;
            this.YBarsRadioButton.Location = new System.Drawing.Point(179, 41);
            this.YBarsRadioButton.Name = "YBarsRadioButton";
            this.YBarsRadioButton.Size = new System.Drawing.Size(39, 17);
            this.YBarsRadioButton.TabIndex = 17;
            this.YBarsRadioButton.Text = "|| Y";
            this.YBarsRadioButton.UseVisualStyleBackColor = true;
            this.YBarsRadioButton.CheckedChanged += new System.EventHandler(this.YBarsRadioButton_CheckedChanged);
            // 
            // XBarsRadioButton
            // 
            this.XBarsRadioButton.AutoSize = true;
            this.XBarsRadioButton.Location = new System.Drawing.Point(179, 18);
            this.XBarsRadioButton.Name = "XBarsRadioButton";
            this.XBarsRadioButton.Size = new System.Drawing.Size(39, 17);
            this.XBarsRadioButton.TabIndex = 16;
            this.XBarsRadioButton.Text = "|| X";
            this.XBarsRadioButton.UseVisualStyleBackColor = true;
            this.XBarsRadioButton.CheckedChanged += new System.EventHandler(this.XBarsRadioButton_CheckedChanged);
            // 
            // YOZBarsRadioButton
            // 
            this.YOZBarsRadioButton.AutoSize = true;
            this.YOZBarsRadioButton.Location = new System.Drawing.Point(15, 64);
            this.YOZBarsRadioButton.Name = "YOZBarsRadioButton";
            this.YOZBarsRadioButton.Size = new System.Drawing.Size(54, 17);
            this.YOZBarsRadioButton.TabIndex = 15;
            this.YOZBarsRadioButton.Text = "|| YOZ";
            this.YOZBarsRadioButton.UseVisualStyleBackColor = true;
            this.YOZBarsRadioButton.CheckedChanged += new System.EventHandler(this.YOZBarsRadioButton_CheckedChanged);
            // 
            // XOYBarsRadioButton
            // 
            this.XOYBarsRadioButton.AutoSize = true;
            this.XOYBarsRadioButton.Location = new System.Drawing.Point(15, 41);
            this.XOYBarsRadioButton.Name = "XOYBarsRadioButton";
            this.XOYBarsRadioButton.Size = new System.Drawing.Size(54, 17);
            this.XOYBarsRadioButton.TabIndex = 14;
            this.XOYBarsRadioButton.Text = "|| XOY";
            this.XOYBarsRadioButton.UseVisualStyleBackColor = true;
            this.XOYBarsRadioButton.CheckedChanged += new System.EventHandler(this.XOYBarsRadioButton_CheckedChanged);
            // 
            // XOZBarsRadioButton
            // 
            this.XOZBarsRadioButton.AutoSize = true;
            this.XOZBarsRadioButton.Checked = true;
            this.XOZBarsRadioButton.Location = new System.Drawing.Point(15, 18);
            this.XOZBarsRadioButton.Name = "XOZBarsRadioButton";
            this.XOZBarsRadioButton.Size = new System.Drawing.Size(54, 17);
            this.XOZBarsRadioButton.TabIndex = 13;
            this.XOZBarsRadioButton.TabStop = true;
            this.XOZBarsRadioButton.Text = "|| XOZ";
            this.XOZBarsRadioButton.UseVisualStyleBackColor = true;
            this.XOZBarsRadioButton.CheckedChanged += new System.EventHandler(this.XOZBarsRadioButton_CheckedChanged);
            // 
            // PanelsPage
            // 
            this.PanelsPage.Controls.Add(this.ZPanelsRadioButton);
            this.PanelsPage.Controls.Add(this.YPanelsRadioButton);
            this.PanelsPage.Controls.Add(this.XPanelsRadioButton);
            this.PanelsPage.Controls.Add(this.YOZPanelsRadioButton);
            this.PanelsPage.Controls.Add(this.XOYPanelsRadioButton);
            this.PanelsPage.Controls.Add(this.XOZPanelsRadioButton);
            this.PanelsPage.Location = new System.Drawing.Point(4, 22);
            this.PanelsPage.Name = "PanelsPage";
            this.PanelsPage.Padding = new System.Windows.Forms.Padding(3);
            this.PanelsPage.Size = new System.Drawing.Size(232, 174);
            this.PanelsPage.TabIndex = 1;
            this.PanelsPage.Text = "Panele";
            this.PanelsPage.UseVisualStyleBackColor = true;
            // 
            // ZPanelsRadioButton
            // 
            this.ZPanelsRadioButton.AutoSize = true;
            this.ZPanelsRadioButton.Location = new System.Drawing.Point(179, 64);
            this.ZPanelsRadioButton.Name = "ZPanelsRadioButton";
            this.ZPanelsRadioButton.Size = new System.Drawing.Size(39, 17);
            this.ZPanelsRadioButton.TabIndex = 24;
            this.ZPanelsRadioButton.Text = "|| Z";
            this.ZPanelsRadioButton.UseVisualStyleBackColor = true;
            // 
            // YPanelsRadioButton
            // 
            this.YPanelsRadioButton.AutoSize = true;
            this.YPanelsRadioButton.Location = new System.Drawing.Point(179, 41);
            this.YPanelsRadioButton.Name = "YPanelsRadioButton";
            this.YPanelsRadioButton.Size = new System.Drawing.Size(39, 17);
            this.YPanelsRadioButton.TabIndex = 23;
            this.YPanelsRadioButton.Text = "|| Y";
            this.YPanelsRadioButton.UseVisualStyleBackColor = true;
            // 
            // XPanelsRadioButton
            // 
            this.XPanelsRadioButton.AutoSize = true;
            this.XPanelsRadioButton.Location = new System.Drawing.Point(179, 18);
            this.XPanelsRadioButton.Name = "XPanelsRadioButton";
            this.XPanelsRadioButton.Size = new System.Drawing.Size(39, 17);
            this.XPanelsRadioButton.TabIndex = 22;
            this.XPanelsRadioButton.Text = "|| X";
            this.XPanelsRadioButton.UseVisualStyleBackColor = true;
            // 
            // YOZPanelsRadioButton
            // 
            this.YOZPanelsRadioButton.AutoSize = true;
            this.YOZPanelsRadioButton.Location = new System.Drawing.Point(15, 64);
            this.YOZPanelsRadioButton.Name = "YOZPanelsRadioButton";
            this.YOZPanelsRadioButton.Size = new System.Drawing.Size(54, 17);
            this.YOZPanelsRadioButton.TabIndex = 21;
            this.YOZPanelsRadioButton.Text = "|| YOZ";
            this.YOZPanelsRadioButton.UseVisualStyleBackColor = true;
            // 
            // XOYPanelsRadioButton
            // 
            this.XOYPanelsRadioButton.AutoSize = true;
            this.XOYPanelsRadioButton.Location = new System.Drawing.Point(15, 41);
            this.XOYPanelsRadioButton.Name = "XOYPanelsRadioButton";
            this.XOYPanelsRadioButton.Size = new System.Drawing.Size(54, 17);
            this.XOYPanelsRadioButton.TabIndex = 20;
            this.XOYPanelsRadioButton.Text = "|| XOY";
            this.XOYPanelsRadioButton.UseVisualStyleBackColor = true;
            // 
            // XOZPanelsRadioButton
            // 
            this.XOZPanelsRadioButton.AutoSize = true;
            this.XOZPanelsRadioButton.Checked = true;
            this.XOZPanelsRadioButton.Location = new System.Drawing.Point(15, 18);
            this.XOZPanelsRadioButton.Name = "XOZPanelsRadioButton";
            this.XOZPanelsRadioButton.Size = new System.Drawing.Size(54, 17);
            this.XOZPanelsRadioButton.TabIndex = 19;
            this.XOZPanelsRadioButton.TabStop = true;
            this.XOZPanelsRadioButton.Text = "|| XOZ";
            this.XOZPanelsRadioButton.UseVisualStyleBackColor = true;
            // 
            // AddCheckBox
            // 
            this.AddCheckBox.AutoSize = true;
            this.AddCheckBox.Location = new System.Drawing.Point(9, 259);
            this.AddCheckBox.Name = "AddCheckBox";
            this.AddCheckBox.Size = new System.Drawing.Size(153, 17);
            this.AddCheckBox.TabIndex = 5;
            this.AddCheckBox.Text = "Dodaj do aktualnej selekcji";
            this.AddCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AddCheckBox.UseVisualStyleBackColor = true;
            // 
            // LevelLabel
            // 
            this.LevelLabel.AutoSize = true;
            this.LevelLabel.Location = new System.Drawing.Point(6, 234);
            this.LevelLabel.Name = "LevelLabel";
            this.LevelLabel.Size = new System.Drawing.Size(60, 13);
            this.LevelLabel.TabIndex = 25;
            this.LevelLabel.Text = "Tolerancja:";
            // 
            // ToleranceTextBox
            // 
            this.ToleranceTextBox.Location = new System.Drawing.Point(171, 234);
            this.ToleranceTextBox.Name = "ToleranceTextBox";
            this.ToleranceTextBox.Size = new System.Drawing.Size(70, 20);
            this.ToleranceTextBox.TabIndex = 26;
            this.ToleranceTextBox.Text = "0,000001";
            // 
            // robotSelectionControl1
            // 
            this.robotSelectionControl1.Location = new System.Drawing.Point(15, 122);
            this.robotSelectionControl1.MaximumSize = new System.Drawing.Size(9999, 26);
            this.robotSelectionControl1.MinimumSize = new System.Drawing.Size(100, 26);
            this.robotSelectionControl1.Name = "robotSelectionControl1";
            this.robotSelectionControl1.Selection = "";
            this.robotSelectionControl1.Size = new System.Drawing.Size(204, 26);
            this.robotSelectionControl1.TabIndex = 26;
            // 
            // GeomFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(254, 331);
            this.Controls.Add(this.ToleranceTextBox);
            this.Controls.Add(this.LevelLabel);
            this.Controls.Add(this.AddCheckBox);
            this.Controls.Add(this.ObjectTabControl);
            this.Controls.Add(this.MainButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(600, 600);
            this.Name = "GeomFilter";
            this.Text = "GeomFilter";
            this.Load += new System.EventHandler(this.GeomFilter_Load);
            this.ObjectTabControl.ResumeLayout(false);
            this.BarsPage.ResumeLayout(false);
            this.BarsPage.PerformLayout();
            this.PanelsPage.ResumeLayout(false);
            this.PanelsPage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button MainButton;
        private System.Windows.Forms.TabControl ObjectTabControl;
        private System.Windows.Forms.TabPage BarsPage;
        private System.Windows.Forms.RadioButton VectorRadioButton;
        private System.Windows.Forms.RadioButton ZBarsRadioButton;
        private System.Windows.Forms.RadioButton YBarsRadioButton;
        private System.Windows.Forms.RadioButton XBarsRadioButton;
        private System.Windows.Forms.RadioButton YOZBarsRadioButton;
        private System.Windows.Forms.RadioButton XOYBarsRadioButton;
        private System.Windows.Forms.RadioButton XOZBarsRadioButton;
        private System.Windows.Forms.TabPage PanelsPage;
        private System.Windows.Forms.CheckBox AddCheckBox;
        private System.Windows.Forms.Label LevelLabel;
        private System.Windows.Forms.RadioButton ZPanelsRadioButton;
        private System.Windows.Forms.RadioButton YPanelsRadioButton;
        private System.Windows.Forms.RadioButton XPanelsRadioButton;
        private System.Windows.Forms.RadioButton YOZPanelsRadioButton;
        private System.Windows.Forms.RadioButton XOYPanelsRadioButton;
        private System.Windows.Forms.RadioButton XOZPanelsRadioButton;
        private System.Windows.Forms.TextBox ToleranceTextBox;
        private ZShared.Controls.RobotSelectionControl robotSelectionControl1;
    }
}

