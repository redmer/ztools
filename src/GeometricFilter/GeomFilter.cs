﻿using System;
using System.Windows.Forms;
using RobotOM;

using MathNet.Spatial.Euclidean;

namespace GeomFilterNamespace
{
    [System.Runtime.InteropServices.ComVisibleAttribute(false)]
    public partial class GeomFilter : Form
    {
        public GeomFilter()
        {
            InitializeComponent();
            this.TopMost = true;
        }

        private void MainButton_Click(object sender, EventArgs e)
        {
            IRobotApplication robapp = default(IRobotApplication);

            try
            {
                robapp = ZShared.RobotHelpers.RobotHelper.RApp;

                RobotNodeServer RNodeServer;
                RNodeServer = robapp.Project.Structure.Nodes;

                double tolerance = Convert.ToDouble(ToleranceTextBox.Text);

                string ObjectText = "";

                if (ObjectTabControl.SelectedTab.Text.ToString() == "Pręty")
                {

                    double XvectS = 0;
                    double YvectS = 0;
                    double ZvectS = 0;

                    if (VectorRadioButton.Checked == true)
                    {
                        //XvectS = Convert.ToDouble(XVectTextBox.Text);
                        //YvectS = Convert.ToDouble(YVectTextBox.Text);
                        //ZvectS = Convert.ToDouble(ZVectTextBox.Text);

                        IRobotBar b = (IRobotBar)robapp.Project.Structure.Bars.Get(robotSelectionControl1.GetRobotSelection().Get(0));
                        int nodeno1 = b.StartNode;
                        int nodeno2 = b.EndNode;
                        IRobotNode n1 = (IRobotNode)robapp.Project.Structure.Nodes.Get(nodeno1);
                        IRobotNode n2 = (IRobotNode)robapp.Project.Structure.Nodes.Get(nodeno2);
                        Point3D p1 = new Point3D(n1.X, n1.Y, n1.Z);
                        Point3D p2 = new Point3D(n2.X, n2.Y, n2.Z);
                        Vector3D v1 = p1.VectorTo(p2);
                        UnitVector3D v = new UnitVector3D(v1.X, v1.Y, v1.Z);
                        XvectS = v.X;
                        YvectS = v.Y;
                        ZvectS = v.Z;

                        double checkSum = Math.Sqrt(XvectS * XvectS + YvectS * YvectS + ZvectS * ZvectS);

                        if (Math.Abs(checkSum - 1) >= tolerance)
                        {
                            string Msg = "Please, check vector's values. Quadratsumme have to be 1";
                            MessageBox.Show(Msg,
                                "Error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                            return;
                        }

                    }

                    RobotBarCollection RBarCollection;
                    RBarCollection = (RobotBarCollection)robapp.Project.Structure.Bars.GetAll();

                    for (int i = 1; i <= RBarCollection.Count; i++)
                    {
                        IRobotBar RBar;
                        RBar = (IRobotBar)RBarCollection.Get(i);

                        IRobotNode StartNode, EndNode;

                        StartNode = (IRobotNode)RNodeServer.Get(RBar.StartNode);
                        EndNode = (IRobotNode)RNodeServer.Get(RBar.EndNode);


                        Double XVect, YVect, ZVect, deltaX, deltaY, deltaZ, BarLengh;
                        Double Xsn, Ysn, Zsn;
                        Double Xen, Yen, Zen;

                        Xsn = StartNode.X;
                        Ysn = StartNode.Y;
                        Zsn = StartNode.Z;

                        Xen = EndNode.X;
                        Yen = EndNode.Y;
                        Zen = EndNode.Z;

                        deltaX = Xen - Xsn;
                        deltaY = Yen - Ysn;
                        deltaZ = Zen - Zsn;

                        BarLengh = NodeToNodeDist(Xsn, Ysn, Zsn,
                            Xen, Yen, Zen);

                        XVect = deltaX / BarLengh;
                        YVect = deltaY / BarLengh;
                        ZVect = deltaZ / BarLengh;

                        if (XOZBarsRadioButton.Checked == true)
                        {
                            if (Math.Abs(YVect) <= tolerance)
                            {
                                ObjectText = ObjectText + RBar.Number.ToString() + " ";
                            }
                        }
                        else if (XOYBarsRadioButton.Checked == true)
                        {
                            if (Math.Abs(ZVect) <= tolerance)
                            {
                                ObjectText = ObjectText + RBar.Number.ToString() + " ";
                            }
                        }
                        else if (YOZBarsRadioButton.Checked == true)
                        {
                            if (Math.Abs(XVect) <= tolerance)
                            {
                                ObjectText = ObjectText + RBar.Number.ToString() + " ";
                            }
                        }
                        else if (XBarsRadioButton.Checked == true)
                        {
                            if (Math.Abs(Math.Abs(XVect) - 1) <= tolerance)
                            {
                                ObjectText = ObjectText + RBar.Number.ToString() + " ";
                            }
                        }
                        else if (YBarsRadioButton.Checked == true)
                        {
                            if (Math.Abs(Math.Abs(YVect) - 1) <= tolerance)
                            {
                                ObjectText = ObjectText + RBar.Number.ToString() + " ";
                            }
                        }
                        else if (ZBarsRadioButton.Checked == true)
                        {
                            if (Math.Abs(Math.Abs(ZVect) - 1) <= tolerance)
                            {
                                ObjectText = ObjectText + RBar.Number.ToString() + " ";
                            }
                        }
                        else
                        {
                            if (((Math.Abs(XVect - XvectS) <= tolerance) && (Math.Abs(YVect - YvectS) <= tolerance) && (Math.Abs(ZVect - ZvectS) <= tolerance)) ||
                                ((Math.Abs(-XVect - XvectS) <= tolerance) && (Math.Abs(-YVect - YvectS) <= tolerance) && (Math.Abs(-ZVect - ZvectS) <= tolerance)))
                            {
                                ObjectText = ObjectText + RBar.Number.ToString() + " ";
                            }
                        }
                    }
                }
                else
                {
                    RobotObjObjectCollection RObjObjectCollection;
                    RObjObjectCollection = (RobotObjObjectCollection)robapp.Project.Structure.Objects.GetAll();

                    for (int i = 1; i <= RObjObjectCollection.Count; i++)
                    {
                        double[] xval;
                        double[] yval;
                        double[] zval;

                        double[] xvect;
                        double[] yvect;
                        double[] zvect;


                        xval = new double[3];
                        yval = new double[3];
                        zval = new double[3];

                        xvect = new double[3];
                        yvect = new double[3];
                        zvect = new double[3];

                        RobotObjObject Obj = (RobotObjObject)RObjObjectCollection.Get(i);

                        if (Obj.Main.Geometry.Type == IRobotGeoObjectType.I_GOT_CONTOUR ||
                            Obj.Main.Geometry.Type == IRobotGeoObjectType.I_GOT_CIRCLE ||
                            Obj.Main.Geometry.Type == IRobotGeoObjectType.I_GOT_POLYLINE)
                        {

                            if (Obj.Main.Geometry.Type == IRobotGeoObjectType.I_GOT_CONTOUR)
                            {
                                RobotGeoContour ContourGeometry = (RobotGeoContour)Obj.Main.GetGeometry();
                                RobotGeoSegmentCollection Segments = (RobotGeoSegmentCollection)ContourGeometry.Segments;

                                for (int j = 1; j <= 3; j++)
                                {
                                    RobotGeoSegment Segment1 = (RobotGeoSegment)Segments.Get(j);
                                    xval[j - 1] = Segment1.P1.X;
                                    yval[j - 1] = Segment1.P1.Y;
                                    zval[j - 1] = Segment1.P1.Z;
                                }
                            }
                            else if (Obj.Main.Geometry.Type == IRobotGeoObjectType.I_GOT_CIRCLE)
                            {
                                RobotGeoCircle CircleGeometry = (RobotGeoCircle)Obj.Main.GetGeometry();
                                xval[0] = CircleGeometry.P1.X;
                                xval[1] = CircleGeometry.P2.X;
                                xval[2] = CircleGeometry.P3.X;

                                yval[0] = CircleGeometry.P1.Y;
                                yval[1] = CircleGeometry.P2.Y;
                                yval[2] = CircleGeometry.P3.Y;

                                zval[0] = CircleGeometry.P1.Z;
                                zval[1] = CircleGeometry.P2.Z;
                                zval[2] = CircleGeometry.P3.Z;
                            }
                            else if (Obj.Main.Geometry.Type == IRobotGeoObjectType.I_GOT_POLYLINE)
                            {
                                RobotGeoPolyline PlineGeometry = (RobotGeoPolyline)Obj.Main.GetGeometry();
                                RobotGeoSegmentCollection Segments = (RobotGeoSegmentCollection)PlineGeometry.Segments;

                                for (int j = 1; j <= 3; j++)
                                {
                                    RobotGeoSegment Segment1 = (RobotGeoSegment)Segments.Get(j);
                                    xval[j - 1] = Segment1.P1.X;
                                    yval[j - 1] = Segment1.P1.Y;
                                    zval[j - 1] = Segment1.P1.Z;
                                }
                            }

                            double A1, B1, C1, D1;
                            double l, n, m;

                            A1 = yval[0] * zval[1] - yval[1] * zval[0] - yval[0] * zval[2] + yval[2] * zval[0] +
                                yval[1] * zval[2] - yval[2] * zval[1];

                            B1 = -xval[0] * zval[1] + xval[1] * zval[0] + xval[0] * zval[2] - xval[2] * zval[0] -
                                xval[1] * zval[2] + xval[2] * zval[1];

                            C1 = xval[0] * yval[1] - xval[1] * yval[0] - xval[0] * yval[2] + xval[2] * yval[0] +
                                xval[1] * yval[2] - xval[2] * yval[1];

                            D1 = -xval[0] * yval[1] * zval[2] + xval[0] * yval[2] * zval[1] + xval[1] * yval[0] * zval[2] -
                                xval[1] * yval[2] * zval[0] - xval[2] * yval[0] * zval[1] + xval[2] * yval[1] * zval[0];


                            if (XOZPanelsRadioButton.Checked == true)
                            {
                                if (Math.Abs(A1) <= tolerance && Math.Abs(C1) <= tolerance)
                                {
                                    ObjectText = ObjectText + Obj.Number.ToString() + " ";
                                }
                            }
                            else if (XOYPanelsRadioButton.Checked == true)
                            {
                                if (Math.Abs(A1) <= tolerance && Math.Abs(B1) <= tolerance)
                                {
                                    ObjectText = ObjectText + Obj.Number.ToString() + " ";
                                }
                            }
                            else if (YOZPanelsRadioButton.Checked == true)
                            {
                                if (Math.Abs(B1) <= tolerance && Math.Abs(C1) <= tolerance)
                                {
                                    ObjectText = ObjectText + Obj.Number.ToString() + " ";
                                }
                            }
                            else if (XPanelsRadioButton.Checked == true)
                            {
                                l = 1;
                                m = 0;
                                n = 0;
                                if (Math.Abs(A1 * l + B1 * m + C1 * n) <= tolerance)
                                {
                                    ObjectText = ObjectText + Obj.Number.ToString() + " ";
                                }
                            }
                            else if (YPanelsRadioButton.Checked == true)
                            {
                                l = 0;
                                m = 1;
                                n = 0;
                                if (Math.Abs(A1 * l + B1 * m + C1 * n) <= tolerance)
                                {
                                    ObjectText = ObjectText + Obj.Number.ToString() + " ";
                                }
                            }
                            else
                            {
                                l = 0;
                                m = 0;
                                n = 1;
                                if (Math.Abs(A1 * l + B1 * m + C1 * n) <= tolerance)
                                {
                                    ObjectText = ObjectText + Obj.Number.ToString() + " ";
                                }
                            }
                        }
                    }
                }

                RobotSelection Rselection;
                Rselection = (RobotSelection)robapp.Project.Structure.Selections.Get(IRobotObjectType.I_OT_OBJECT);

                if (AddCheckBox.Checked == true)
                {
                    Rselection.AddText(ObjectText);
                }
                else
                {
                    Rselection.FromText(ObjectText);
                }

                robapp.Project.ViewMngr.Refresh();
                robapp.Interactive = 1;
                robapp = null;

            }
            catch (Exception exp)
            {
                string excTypeName = exp.GetType().Name.ToUpper();
                string Msg;

                switch (excTypeName)
                {
                    case "COMEXCEPTION":

                        Msg = "Please, start Robot and load model";
                        break;

                    case "FORMATEXCEPTION":

                        Msg = "Please, check input values";
                        robapp.Interactive = 1;
                        break;

                    default:
                        Msg = "Some error occured";
                        robapp.Interactive = 1;
                        break;
                }

                MessageBox.Show(Msg,
                                "Error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        public static double NodeToNodeDist(double X1, double Y1, double Z1,
            double X2, double Y2, double Z2)
        {
            return Math.Sqrt((X2 - X1) * (X2 - X1) + (Y2 - Y1) * (Y2 - Y1) + (Z2 - Z1) * (Z2 - Z1));
        }



        private void VectorRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            //XVectTextBox.Enabled = VectorRadioButton.Checked;
            //YVectTextBox.Enabled = VectorRadioButton.Checked;
            //ZVectTextBox.Enabled = VectorRadioButton.Checked;

            //if (XVectTextBox.Text == "")
            //{
            //    XVectTextBox.Text = "0";
            //}
            //if (YVectTextBox.Text == "")
            //{
            //    YVectTextBox.Text = "0";
            //}
            //if (ZVectTextBox.Text == "")
            //{
            //    ZVectTextBox.Text = "0";
            //}

            robotSelectionControl1.Enabled = VectorRadioButton.Checked;

            MainButton.Enabled = true;
        }

        private void XOZBarsRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            //XVectTextBox.Enabled = VectorRadioButton.Checked;
            //YVectTextBox.Enabled = VectorRadioButton.Checked;
            //ZVectTextBox.Enabled = VectorRadioButton.Checked;
            robotSelectionControl1.Enabled = false;
        }

        private void XOYBarsRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            //XVectTextBox.Enabled = VectorRadioButton.Checked;
            //YVectTextBox.Enabled = VectorRadioButton.Checked;
            //ZVectTextBox.Enabled = VectorRadioButton.Checked;
            robotSelectionControl1.Enabled = false;
        }

        private void YOZBarsRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            //XVectTextBox.Enabled = VectorRadioButton.Checked;
            //YVectTextBox.Enabled = VectorRadioButton.Checked;
            //ZVectTextBox.Enabled = VectorRadioButton.Checked;
            robotSelectionControl1.Enabled = false;
        }

        private void XBarsRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            //XVectTextBox.Enabled = VectorRadioButton.Checked;
            //YVectTextBox.Enabled = VectorRadioButton.Checked;
            //ZVectTextBox.Enabled = VectorRadioButton.Checked;
            robotSelectionControl1.Enabled = false;
        }

        private void YBarsRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            //XVectTextBox.Enabled = VectorRadioButton.Checked;
            //YVectTextBox.Enabled = VectorRadioButton.Checked;
            //ZVectTextBox.Enabled = VectorRadioButton.Checked;
            robotSelectionControl1.Enabled = false;
        }

        private void ZBarsRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            //XVectTextBox.Enabled = VectorRadioButton.Checked;
            //YVectTextBox.Enabled = VectorRadioButton.Checked;
            //ZVectTextBox.Enabled = VectorRadioButton.Checked;
            robotSelectionControl1.Enabled = false;
        }

        private void GeomFilter_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            this.robotSelectionControl1.ObjectType = IRobotObjectType.I_OT_BAR;
            this.robotSelectionControl1.Enabled = false;
        }
    }

}
