﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using RobotOM;

using ZShared.Kernel;

namespace GeomFilterNamespace
{
    //[System.Runtime.InteropServices.ComVisibleAttribute(true), System.Runtime.InteropServices.Guid("8BE3FAFE-089C-47CF-AA84-123D2E90D53E")]
    public class Class1 : IPlugin
    {



        //private IRobotApplication iapp = null;
        //public bool Connect(RobotApplication robot_app, int add_in_id, bool first_time)
        //{
        //    iapp = robot_app;
        //    return true;
        //}

        //public bool Disconnect()
        //{
        //    iapp = null;
        //    return true;
        //}

        //public void DoCommand(int cmd_id)
        //{
        //    Form GeomFilter = new GeomFilter();
        //    GeomFilter.Show();
        //}


        private RobotApplication _rapp;
        private Form _geofilter;

        public Class1()
        {
            //_rapp = new RobotApplication();

        }

        public Form MainInterface
        {
            get
            {
                _rapp = ZShared.RobotHelpers.RobotHelper.RApp;
                _geofilter = new GeomFilter();
                return _geofilter;
            }
        }

        public string MenuName
        {
            get
            {
                return "Filtr Geometryczny";
            }
        }

        public event PluginError PluginError;
    }
}
