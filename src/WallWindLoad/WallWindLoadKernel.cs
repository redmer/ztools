﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using MathNet.Spatial.Euclidean;
using ZShared.Helpers;
using ZShared.Kernel;
using ClipperLib;

namespace ZTools.WallWindLoad
{
    public enum Area { A, B, C };

    public class WallWindLoadKernel
    {
        List<Point3D> ProjectionPlanePoints;
        double _La = 0;
        double _Lb = 0;

        public double La
        {
            get
            {
                return _La;
            }
            set
            {
                _La = value;
                hasChanged = true;
            }
        }
        public double Lb
        {
            get
            {
                return _Lb;
            }
            set
            {
                _Lb = value;
                hasChanged = true;
            }
        }

        protected bool hasChanged = true;

        protected Dictionary<Area, List<int>> PointsMatrix = new Dictionary<Area, List<int>>()
        {
            { Area.A, new List<int>() { 0, 1 ,6 ,7} },
            { Area.B, new List<int>() { 1, 2, 5, 6} },
            { Area.C, new List<int>() { 2, 3, 4, 5 } }
        };

        public Plane ProjectionPlane { get; private set; }

        Transformation TR;

        List<List<Point3D>> _panelPoints = new List<List<Point3D>>();
        public IList<List<Point3D>> PanelPoints {
            get
            {
                return _panelPoints.AsReadOnly();
            }
        }
        public void AddPanelPath(List<Point3D> points)
        {
            _panelPoints.Add(points);
            hasChanged = true;
        }
        public void AddPanelPaths(IList<List<Point3D>> paths)
        {
            _panelPoints.AddRange(paths);
        }

        List<Point3D> _userLoadPoints { get; } = new List<Point3D>();
        public IList<Point3D> UserLoadPoints
        {
            get
            {
                return _userLoadPoints.AsReadOnly();
            }
        }
        public void AddUserLoadPoint(Point3D p)
        {
            _userLoadPoints.Add(p);
            hasChanged = true;
        }
        public void AddUserLoadPoints(IList<Point3D> pts)
        {
            _userLoadPoints.AddRange(pts);
        }

        List<Point3D> LoadPointsInLCS;

        /// <summary>
        /// Konstruktor kernela geometrycznego dla wallwindloada
        /// Po zainicjowaniu obiektu należy:
        /// 1. Ustawić płaszczyznę rzutowania (na tej płaszczyźnie będzie rozłożona matryca obciążeń)
        /// składa się z dwóch punktów ponieważ płąszczyzna zawsze jest pionowa.
        /// Uwaga! płaszczyzna rzutowania zazwyczaj jest prostopadła do kierunku wiatru i równoległa
        /// do ściany. SetProjectionPlane()
        /// 
        /// 2. Ustawić punkty paneli (kernel obliczeniowy nie może sam ich pobrać z paneli - może powinno się
        /// extension napisać ale to chyba bez sensu List<List<Point3D>> PanelPoints = ePanelPoints.Current;
        /// 
        /// 3. Ustawić punkty obciążenia użytkownika ponieważ nie zawsze się to zgadza z punktami panelu.
        /// List<Point3D> UserLoadPoints = eLoadPoints.Current;
        /// 
        /// 4. Dodać La i Lb
        /// 
        /// 5. Wynik działania metody (tzn punkty w układzie współrzędnych odpowiadajęce matrycy obciążeń
        /// najłatwiej uzyskać przez public List<Point3D> GetLoadAreaPolygonInGCS(Area area, List<Point3D> polygon);
        /// 
        /// </summary>
        public WallWindLoadKernel()
        {
        }

        public void SetProjectionPlane(Point3D p1, Point3D p2)
        {
            ProjectionPlanePoints = new List<Point3D>();

            Vector3D v = new Vector3D(0, 0, 1);
            ProjectionPlanePoints.Clear();
            ProjectionPlanePoints.Add(p1);
            ProjectionPlanePoints.Add(p2);
            Point3D p3 = p1 + v;
            ProjectionPlanePoints.Add(p3);

            ProjectionPlane = new Plane(p1, p2, p3);

            TR = new Transformation(p1.ToVector().ToArray(), p2.ToVector().ToArray(), p3.ToVector().ToArray());
            hasChanged = true;
        }

        public List<Point3D> GetProjectionPlanePointsInLCS()
        {
            List<Point3D> res = new List<Point3D>();
            foreach (Point3D p in ProjectionPlanePoints)
            {
                res.Add(new Point3D(TR.TransformPoint(p.ToVector().ToArray())));
            }
            return res;
        }

        public List<List<Point3D>> GetPanelPointsInLCS()
        {
            List<List<Point3D>> results = new List<List<Point3D>>();
            foreach (List<Point3D> list in PanelPoints)
            {
                List<Point3D> CurrentList = new List<Point3D>();
                foreach (Point3D p in list)
                {
                    Point3D transformed = new Point3D(TR.TransformPoint(p.ToVector().ToArray()));
                    CurrentList.Add(transformed);
                }
                results.Add(CurrentList);
            }
            return results;
        }

        public List<Point3D> GetUserLoadPointsInLCS()
        {
            List<Point3D> result = new List<Point3D>();
            foreach (Point3D p in UserLoadPoints)
            {
                Point3D pprim = new Point3D(TR.TransformPoint(p.ToVector().ToArray()));
                result.Add(pprim);
            }
            return result;
        }

        public List<Point3D> GetProjectedLoadPointsInLCS()
        {
            List<Point3D> pts = new List<Point3D>();
            Plane p = new Plane(ProjectionPlanePoints[0], ProjectionPlanePoints[1], ProjectionPlanePoints[2]);

            foreach (Point3D point in UserLoadPoints)
            {
                Point3D projected = p.Project(point);
                pts.Add(new Point3D(TR.TransformPoint(projected.ToVector().ToArray())));
            }
            return pts;
        }

        private List<List<Point3D>> GetProjectedPanelPointsInLCS()
        {
            List<List<Point3D>> ppLCS = GetPanelPointsInLCS();
            List<List<Point3D>> ppLCSprojected = new List<List<Point3D>>();
            foreach (List<Point3D> l1 in ppLCS)
            {
                List<Point3D> currentList = new List<Point3D>();
                foreach (Point3D p in l1)
                {
                    currentList.Add(new Point3D(p.X, p.Y, 0));
                }
                ppLCSprojected.Add(currentList);
            }
            return ppLCSprojected;
        }

        public List<List<Point3D>> GetPanelsOutlineInLCS()
        {
            Clipper clipper = new Clipper();
            List<List<Point3D>> Panels = GetProjectedPanelPointsInLCS();
            int z = 0;
            foreach (List<Point3D> path in Panels)
            {
                PolyType type;
                if (z == 0)
                    type = PolyType.ptSubject;
                else
                    type = PolyType.ptClip;
                z++;
                clipper.AddPath(path.ToIntPointList(), type, true);
            }
            List<List<IntPoint>> resultsI = new List<List<IntPoint>>();
            clipper.Execute(ClipType.ctUnion, resultsI);

            List<List<Point3D>> results = new List<List<Point3D>>();
            foreach (List<IntPoint> resi in resultsI)
            {
                results.Add(resi.ToPoint3DList());
            }
            return results;
        }

        public List<List<Point3D>> GetLoadPolygon()
        {
            List<IntPoint> Subject = GetProjectedLoadPointsInLCS().ToIntPointList();
            List<List<IntPoint>> Clips = new List<List<IntPoint>>();
            foreach (List<Point3D> lst in GetPanelsOutlineInLCS())
            {
                Clips.Add(lst.ToIntPointList());
            }
            Clipper clipper = new Clipper();
            clipper.AddPath(Subject, PolyType.ptSubject, true);
            clipper.AddPaths(Clips, PolyType.ptClip, true);
            List<List<IntPoint>> solution = new List<List<IntPoint>>();
            clipper.Execute(ClipType.ctIntersection, solution);

            List<List<Point3D>> Solution = new List<List<Point3D>>();
            foreach (List<IntPoint> lst in solution)
            {
                Solution.Add(lst.ToPoint3DList());
            }
            return Solution;
        }

        private double[] MinX()
        {
            var LoadPolys = GetLoadPolygon();
            double[] res = new double[LoadPolys.Count];
            for (int i = 0; i < LoadPolys.Count; i++)
            {
                var Poly = LoadPolys[i];
                res[i] = (from a in Poly select a.X).Min();
            }
            return res;
        }

        private double[] MaxX()
        {
            var LoadPolys = GetLoadPolygon();
            double[] res = new double[LoadPolys.Count];
            for (int i = 0; i < LoadPolys.Count; i++)
            {
                var Poly = LoadPolys[i];
                res[i] = (from a in Poly select a.X).Max();
            }
            return res;
        }

        private double[] MinY()
        {
            var LoadPolys = GetLoadPolygon();
            double[] res = new double[LoadPolys.Count];
            for (int i = 0; i < LoadPolys.Count; i++)
            {
                var Poly = LoadPolys[i];
                res[i] = (from a in Poly select a.Y).Min();
            }
            return res;
        }

        private double[] MaxY()
        {
            var LoadPolys = GetLoadPolygon();
            double[] res = new double[LoadPolys.Count];
            for (int i = 0; i < LoadPolys.Count; i++)
            {
                var Poly = LoadPolys[i];
                res[i] = (from a in Poly select a.Y).Max();
            }
            return res;
        }

        private void PopulatePoints()
        {
            if (LoadPointsInLCS == null)
                LoadPointsInLCS = new List<Point3D>();
            LoadPointsInLCS.Clear();

            Vector3D h1 = new Vector3D(La, 0, 0);
            Vector3D h2 = new Vector3D(Lb, 0, 0);

            //to jest brzydkie!! i bedzie generowalo bledy / problemy
            Vector3D v1 = new Vector3D(0, MaxY()[0] - MinY()[0], 0);


            LoadPointsInLCS.Add(new Point3D( MinX()[0], MinY()[0], 0 )); //p0
            LoadPointsInLCS.Add(LoadPointsInLCS[0] + h1); //p1
            LoadPointsInLCS.Add(LoadPointsInLCS[1] + h2); //p2
            LoadPointsInLCS.Add(new Point3D( MaxX()[0], MinY()[0], 0 )); //p3
            LoadPointsInLCS.Add(LoadPointsInLCS[3] + v1); //p4
            LoadPointsInLCS.Add(LoadPointsInLCS[2] + v1); //p5
            LoadPointsInLCS.Add(LoadPointsInLCS[1] + v1); //p6
            LoadPointsInLCS.Add(LoadPointsInLCS[0] + v1); //p7
        }

        public List<Point3D> GetLoadAreaPolygon(Area area)
        {
            if (hasChanged)
                PopulatePoints();

            List<Point3D> result = new List<Point3D>();
            foreach (int p in PointsMatrix[area])
                result.Add(LoadPointsInLCS[p]);

            hasChanged = false;
            return result;
        }

        /// <summary>
        /// Metoda wykonuje rzutowanie odwrotne z matrycy obciążeniowej (GetLoadAreaPolygon).
        /// 1. Tworzy plasczyzne z panelu PLANE
        /// 2. Tworzy plaszczyzne z GetLoadAreaPolygon ORIG_PLANE
        /// 3. Tworzy prosta prostopadla PROSTA do ORIG_PLANE
        /// 4. Punkt przeciecia PROSTA z PLANE jest szukanym punktem w ten sposób powstaje PROJECTED_POLYGON
        /// 5. Szukane jest przecięcie (bool &) POLYGON z PROJECTED_POLYGON
        /// 6. zwracany jest wynik przeciecia
        /// na panel
        /// </summary>
        /// <param name="area">Typ pola A, B, C zgodnie z PN-EN</param>
        /// <param name="polygon">Punkty tworzące panel w globalnym ukladzie wspolrzednych</param>
        /// <returns></returns>
        public List<Point3D> GetLoadAreaPolygonInGCS(Area area, List<Point3D> polygon)
        {
            if (polygon.Count < 3)
                throw (new InvalidOperationException("Figura nie może mieć mniej niż 3 punkty"));

            List<Point3D> loadPointsLCS = GetLoadAreaPolygon(area);
            List<Point3D> loadPointsGCS = TR.UnTransformPoints(loadPointsLCS);
            Plane origPlane = new Plane(loadPointsGCS[0], loadPointsGCS[1], loadPointsGCS[2]);
            Plane plane = new Plane(polygon[0], polygon[1], polygon[2]);

            List<Point3D> loadUnProjectedPolygon = new List<Point3D>();
            foreach (Point3D p in loadPointsGCS)
            {
                Ray3D r = new Ray3D(p, origPlane.Normal);
                Point3D pt = plane.IntersectionWith(r);
                loadUnProjectedPolygon.Add(pt);
            }
            Transformation tr = new Transformation(polygon[0].ToArray(), polygon[1].ToArray(), polygon[2].ToArray());
            var trLoads = tr.TransformPoints(loadUnProjectedPolygon);
            var trPolygon = tr.TransformPoints(polygon);


//teraz jest za wczesnie punkty mają zety!!!!
            Clipper clip = new Clipper();
            List<IntPoint> l1 = trLoads.ToIntPointList();
            List<IntPoint> l2 = trPolygon.ToIntPointList();

            clip.AddPath(l1, PolyType.ptSubject, true);
            clip.AddPath(l2, PolyType.ptClip, true);

            List<List<IntPoint>> results = new List<List<IntPoint>>();

            if (!clip.Execute(ClipType.ctIntersection, results))
            {
                throw (new InvalidOperationException("Blad podczas przeciecia figur!"));
            }
            if (results.Count == 0)
            {
                return new List<Point3D>();
            }
            else if (results.Count == 1)
            {
                return tr.UnTransformPoints(results[0].ToPoint3DList());
                //return results[0].ToPoint3DList();
            }
            else
            {
                Console.WriteLine("Jest więcej niż jeden rezultat z przecięcia figur ale nie mogę go zwrócić");
                return tr.UnTransformPoints(results[0].ToPoint3DList());
            }
        }
    }
}
