﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using MathNet.Spatial.Euclidean;

namespace ZTools.WallWindLoad 
{
    public partial class UI : Form
    {
        public WallWindLoad Kernel { get; set; }

        public double ALength
        {
            get
            {
                return aLength.Value;
            }
            set
            {
                aLength.Value = value;
            }
        }

        public double BLength
        {
            get
            {
                return bLength.Value;
            }
            set
            {
                bLength.Value = value;
            }
        }

        public double AValue
        {
            get
            {
                return AVal.Value;
            }
            set
            {
                AVal.Value = value;
            }
        }

        public double BValue
        {
            get
            {
                return BVal.Value;
            }
            set
            {
                BVal.Value = value;
            }
        }

        public double CValue
        {
            get
            {
                return CVal.Value;
            }
            set
            {
                CVal.Value = value;
            }
        }

        public RobotOM.RobotSelection Panels
        {
            get
            {
                return PanelsControl.GetRobotSelection();
            }
        }

        public int LoadCase
        {
            get
            {
                return robotCaseControl1.LoadCaseNo;
            }
        }

        public List<Point3D> UserLoadPoints
        {
            get
            {
                IList<ZShared.Controls.ZPointControl> points = zLoadPointList.ZPointControlList;
                List<Point3D> pts = new List<Point3D>();
                foreach (var contr in points)
                {
                    pts.Add(contr.GetPoint3D());
                }
                return pts;
            }
        }

        public List<Point3D> LoadProjectionPoints
        {
            get
            {
                IList<ZShared.Controls.ZPointControl> UserLoads = zProjectMatrixPointList.ZPointControlList;
                if (UserLoads.Count < 2)
                {
                    IList<ZShared.Controls.ZPointControl> Points = zLoadPointList.ZPointControlList;
                    return new List<Point3D>()
                    {
                        Points[0].GetPoint3D(),
                        Points[1].GetPoint3D()
                    };
                }
                else
                {
                    return new List<Point3D>()
                    {
                        UserLoads[0].GetPoint3D(),
                        UserLoads[1].GetPoint3D()
                    };
                }
            }
        }

        public UI() 
        {
            InitializeComponent();

            Text = "Generator ssania wiatru dla EC3";

            PictureBox box = new PictureBox();
            
            groupBox5.Controls.Add(box);
            box.Image = Resource1.WallWindLoadSchema;
            box.Dock = DockStyle.Fill;
            box.SizeMode = PictureBoxSizeMode.Zoom;

            //zPointControl3.X = 0;
            //zPointControl3.Y = 0;
            //zPointControl3.Z = 5.58;

            //zPointControl4.X = 0;
            //zPointControl4.Y = 66.0;
            //zPointControl4.Z = 3.6;

            this.TopMost = true;
        }

        private void InitializeTestData()
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Kernel.GenerateLoad();
        }
    }
}
