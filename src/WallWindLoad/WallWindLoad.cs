﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

using ZShared.Kernel;
using ZShared.RobotHelpers;

using RobotOM;
using MathNet.Spatial.Euclidean;

namespace ZTools.WallWindLoad
{
    public class WallWindLoad : IPlugin
    {
        #region Private 
        private UI _ui;

        public event PluginError PluginError;
        #endregion

        #region IPlugin
        public System.Windows.Forms.Form MainInterface
        {
            get
            {
                if (_ui != null)
                    return _ui;
                else
                    throw (new NullReferenceException("Couldn't create an interface window"));
            }
        }
        public string MenuName
        {
            get
            {
                return "Obciążenie wiatrem na ścianę EC3";
            }
        }
        #endregion

        public WallWindLoad()
        {
            PluginError += OnPluginError;

            _ui = new UI();
            _ui.Kernel = this;
        }

        public void GenerateLoad()
        {
            #region ZAMYKANIE TABEL ROBOTA
            // Zamykam tabele jesli istnieje przed rozpoczeciem
            bool OpenedLoadTable = false;
            int n = RobotHelper.RApp.Project.ViewMngr.TableCount;
            for (int i = 1; i <= n; i++)
            {
                RobotTableFrame t = RobotHelper.RApp.Project.ViewMngr.GetTable(i);
                if (t != null)
                {
                    OpenedLoadTable = true;

                    RobotTable table = t.Get(1);
                    IRobotTableDataType type = table.GetDataType(1);
                    if ((int)type == 279)

                    t.Window.SendMessage(16, 0, 0);
                }
            }
            #endregion

            Dictionary<Area, double> LoadValues = new Dictionary<Area, double>()
            {
                { Area.A, _ui.AValue },
                { Area.B, _ui.BValue },
                { Area.C, _ui.CValue }
            };

            WallWindLoadKernel k = new WallWindLoadKernel();
            k.La = _ui.ALength;
            k.Lb = _ui.BLength;

            k.SetProjectionPlane(_ui.LoadProjectionPoints[0], _ui.LoadProjectionPoints[1]);

            List<List<Point3D>> PanelPaths = CalculatePanelPahts(_ui.Panels);

            k.AddPanelPaths(PanelPaths);
            k.AddUserLoadPoints(_ui.UserLoadPoints);

            UnitVector3D WindDirection = k.ProjectionPlane.Normal;

            RobotObjObjectCollection objs = RobotHelper.RStructure.Objects.GetMany(_ui.Panels) as RobotObjObjectCollection;


            for (int i = 1; i <= objs.Count; i++)
            {
                RobotObjObject obj = objs.Get(i);
                int PanelNo = obj.Number;
                List<Point3D> PanelPath = RobotHelper.PathFromCladding(obj);
                foreach (Area area in Enum.GetValues(typeof(Area)))
                {
                    List<Point3D> loadPath = k.GetLoadAreaPolygonInGCS(area, PanelPath);
                    AddLoadToRobot(
                        CaseHelpers.SimpleCaseFromNumber(_ui.LoadCase), 
                        loadPath, 
                        PanelNo, 
                        LoadValues[area] * WindDirection.X,
                        LoadValues[area] * WindDirection.Y,
                        LoadValues[area] * WindDirection.Z
                        );
                }
            }

            #region OTWIERANIE TABEL I REFRESH
            RobotHelper.RApp.Project.ViewMngr.Refresh();
            if (OpenedLoadTable)
            {
                RobotHelper.RApp.Project.ViewMngr.CreateTable(IRobotTableType.I_TT_LOADS, IRobotTableDataType.I_TDT_DEFAULT);
            }
            #endregion
        }
        
        protected List<List<Point3D>> CalculatePanelPahts(RobotSelection sel)
        {
            RobotObjObjectCollection objs = RobotHelper.RStructure.Objects.GetMany(sel) as RobotObjObjectCollection;
            List<List<Point3D>> results = new List<List<Point3D>>();
            for (int i = 1; i <= objs.Count; i++)
            {
                RobotObjObject obj = objs.Get(i);
                results.Add(RobotHelper.PathFromCladding(obj));
            }
            return results;
        }

        protected void AddLoadToRobot(IRobotSimpleCase c, List<Point3D> contour, int panelNo, double valX, double valY, double valZ)
        {
            RobotStructure rs = RobotHelper.RStructure;
            int loadNo = c.Records.New(IRobotLoadRecordType.I_LRT_IN_CONTOUR);
            RobotLoadRecordInContour load = c.Records.Get(loadNo) as RobotLoadRecordInContour;
            load.SetValue((short)IRobotInContourRecordValues.I_ICRV_LOCAL, 0);
            load.SetValue((short)IRobotInContourRecordValues.I_ICRV_NPOINTS, contour.Count);
            load.SetValue((short)IRobotInContourRecordValues.I_ICRV_PX1, valX * 1000);
            load.SetValue((short)IRobotInContourRecordValues.I_ICRV_PY1, valY * 1000);
            load.SetValue((short)IRobotInContourRecordValues.I_ICRV_PZ1, valZ * 1000);

            for (int i = 0; i < contour.Count; i++)
            {
                load.SetContourPoint(i+1, contour[i].X, contour[i].Y, contour[i].Z);
            }

            load.Objects.AddOne(panelNo);
        }

        protected void OnPluginError(object sender, PluginErrorEventArgs args)
        {
        }

    }
}
