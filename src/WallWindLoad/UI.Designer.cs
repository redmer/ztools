﻿namespace ZTools.WallWindLoad
{
    partial class UI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UI));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.PanelsControl = new ZShared.Controls.RobotSelectionControl();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AVal = new ZShared.Controls.ZDoubleTextBox();
            this.BVal = new ZShared.Controls.ZDoubleTextBox();
            this.CVal = new ZShared.Controls.ZDoubleTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.aLength = new ZShared.Controls.ZDoubleTextBox();
            this.bLength = new ZShared.Controls.ZDoubleTextBox();
            this.cLength = new ZShared.Controls.ZDoubleTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.zLoadPointList = new ZShared.Controls.ZPointList();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.robotCaseControl1 = new ZShared.Controls.RobotCaseControl();
            this.zProjectMatrixPointList = new ZShared.Controls.ZPointList();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.PanelsControl);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 79);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Panele";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(315, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Wybierz panele/okładziny do których ma zostać przyłożone obciążenie";
            // 
            // PanelsControl
            // 
            this.PanelsControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelsControl.Location = new System.Drawing.Point(5, 47);
            this.PanelsControl.MaximumSize = new System.Drawing.Size(9999, 26);
            this.PanelsControl.MinimumSize = new System.Drawing.Size(100, 26);
            this.PanelsControl.Name = "PanelsControl";
            this.PanelsControl.ObjectType = RobotOM.IRobotObjectType.I_OT_GEOMETRY;
            this.PanelsControl.Selection = "243";
            this.PanelsControl.Size = new System.Drawing.Size(316, 26);
            this.PanelsControl.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(623, 534);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Generuj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(542, 534);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Zakończ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel1);
            this.groupBox2.Location = new System.Drawing.Point(12, 175);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(327, 100);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Wartości obciążenia";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.AVal, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.BVal, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.CVal, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(321, 81);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 27);
            this.label2.TabIndex = 0;
            this.label2.Text = "Strefa A [kN/m2]";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 27);
            this.label3.TabIndex = 1;
            this.label3.Text = "Strefa B [kN/m2]";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 27);
            this.label4.TabIndex = 2;
            this.label4.Text = "Strefa C [kN/m2]";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AVal
            // 
            this.AVal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AVal.Location = new System.Drawing.Point(163, 3);
            this.AVal.Name = "AVal";
            this.AVal.Size = new System.Drawing.Size(155, 21);
            this.AVal.TabIndex = 3;
            this.AVal.Value = 2D;
            // 
            // BVal
            // 
            this.BVal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BVal.Location = new System.Drawing.Point(163, 30);
            this.BVal.Name = "BVal";
            this.BVal.Size = new System.Drawing.Size(155, 21);
            this.BVal.TabIndex = 4;
            this.BVal.Value = 1D;
            // 
            // CVal
            // 
            this.CVal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CVal.Location = new System.Drawing.Point(163, 57);
            this.CVal.Name = "CVal";
            this.CVal.Size = new System.Drawing.Size(155, 21);
            this.CVal.TabIndex = 5;
            this.CVal.Value = 0.5D;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel2);
            this.groupBox4.Location = new System.Drawing.Point(12, 281);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(327, 100);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Długości stref";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.aLength, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.bLength, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.cLength, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(321, 81);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 27);
            this.label5.TabIndex = 0;
            this.label5.Text = "Strefa A [m]";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 27);
            this.label6.TabIndex = 1;
            this.label6.Text = "Strefa B [m]";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(154, 27);
            this.label7.TabIndex = 2;
            this.label7.Text = "Strefa C [m]";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // aLength
            // 
            this.aLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aLength.Location = new System.Drawing.Point(163, 3);
            this.aLength.Name = "aLength";
            this.aLength.Size = new System.Drawing.Size(155, 21);
            this.aLength.TabIndex = 3;
            this.aLength.Value = 6D;
            // 
            // bLength
            // 
            this.bLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bLength.Location = new System.Drawing.Point(163, 30);
            this.bLength.Name = "bLength";
            this.bLength.Size = new System.Drawing.Size(155, 21);
            this.bLength.TabIndex = 4;
            this.bLength.Value = 12D;
            // 
            // cLength
            // 
            this.cLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cLength.Location = new System.Drawing.Point(163, 57);
            this.cLength.Name = "cLength";
            this.cLength.Size = new System.Drawing.Size(155, 21);
            this.cLength.TabIndex = 5;
            this.cLength.Value = 50D;
            // 
            // groupBox5
            // 
            this.groupBox5.Location = new System.Drawing.Point(345, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(601, 369);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Schemat";
            // 
            // zLoadPointList
            // 
            this.zLoadPointList.GroupBoxText = "Lista punktów obciążenia (dowolna ilość punktów)";
            this.zLoadPointList.Location = new System.Drawing.Point(17, 384);
            this.zLoadPointList.Name = "zLoadPointList";
            this.zLoadPointList.Padding = new System.Windows.Forms.Padding(4);
            this.zLoadPointList.Size = new System.Drawing.Size(484, 141);
            this.zLoadPointList.TabIndex = 11;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.robotCaseControl1);
            this.groupBox3.Location = new System.Drawing.Point(12, 97);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(327, 54);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Przypadek";
            // 
            // robotCaseControl1
            // 
            this.robotCaseControl1.Location = new System.Drawing.Point(7, 20);
            this.robotCaseControl1.MaximumSize = new System.Drawing.Size(2000, 27);
            this.robotCaseControl1.MinimumSize = new System.Drawing.Size(150, 27);
            this.robotCaseControl1.Name = "robotCaseControl1";
            this.robotCaseControl1.Size = new System.Drawing.Size(314, 27);
            this.robotCaseControl1.TabIndex = 0;
            // 
            // zProjectMatrixPointList
            // 
            this.zProjectMatrixPointList.GroupBoxText = "Definicja płaszczyzny rzutowania (tylko 2 punkty są brane pod uwagę)";
            this.zProjectMatrixPointList.Location = new System.Drawing.Point(502, 387);
            this.zProjectMatrixPointList.Name = "zProjectMatrixPointList";
            this.zProjectMatrixPointList.Padding = new System.Windows.Forms.Padding(4);
            this.zProjectMatrixPointList.Size = new System.Drawing.Size(444, 141);
            this.zProjectMatrixPointList.TabIndex = 13;
            // 
            // UI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 714);
            this.Controls.Add(this.zProjectMatrixPointList);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.zLoadPointList);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UI";
            this.Text = "WallWindLoad";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private ZShared.Controls.RobotSelectionControl PanelsControl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private ZShared.Controls.ZDoubleTextBox AVal;
        private ZShared.Controls.ZDoubleTextBox BVal;
        private ZShared.Controls.ZDoubleTextBox CVal;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private ZShared.Controls.ZDoubleTextBox aLength;
        private ZShared.Controls.ZDoubleTextBox bLength;
        private ZShared.Controls.ZDoubleTextBox cLength;
        private System.Windows.Forms.GroupBox groupBox5;
        private ZShared.Controls.ZPointList zLoadPointList;
        private System.Windows.Forms.GroupBox groupBox3;
        private ZShared.Controls.RobotCaseControl robotCaseControl1;
        private ZShared.Controls.ZPointList zProjectMatrixPointList;
    }
}