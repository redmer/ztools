﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using MathNet.Spatial.Euclidean;
using RWL = RoofWindLoad.RoofWindLoad;
using RoofWindLoad;

namespace RoofWindLoadTests
{
    [TestClass]
    public class RoofWindLoadTestsLoadGeometry
    {
        List<List<Point3D>> DemoRects = new List<List<Point3D>>();
        List<List<Point3D>> TransformResults = new List<List<Point3D>>();
        List<CoordinateSystem> TransformedCS = new List<CoordinateSystem>();
        List<Dictionary<Areas, List<Point3D>>> TransformedAreaResults = new List<Dictionary<Areas, List<Point3D>>>();
        List<Dictionary<Areas, List<Point3D>>> AreaResults = new List<Dictionary<Areas, List<Point3D>>>();

        List<double> Lf = new List<double>();
        List<double> Lh = new List<double>();
        List<double> Bf = new List<double>();

        List<LoadGeometry> LGs = new List<LoadGeometry>();
        List<List<double>> ExtremeXYs = new List<List<double>>();

        List<IList> lists = new List<IList>();

        [TestInitialize]
        public void Initialize()
        {
            #region Dane
            //przed transformacja
            List<Point3D> R1 = new List<Point3D>()
            {
            new Point3D(6181.77297563, 8072.13808835, 0),
            new Point3D(9591.10803313, 5597.51003976, 0),
            new Point3D(11277.83167964, 9255.65584285, 0),
            new Point3D(5320.46731950, 10797.81530549, 0)
            };

            DemoRects.Add(R1);
            //rezultaty po transformacji
            List<Point3D> Res1 = new List<Point3D>()
            {
            new Point3D(0, 0, 0),
            new Point3D(4212.76031992, 0, 0),
            new Point3D(3428.96444762, 3951.29490255, 0),
            new Point3D(-2298.14091626, 1699.91530799, 0)
            };
            TransformResults.Add(Res1);

            TransformedCS.Add(new CoordinateSystem(
                new Point3D(6181.77297563, 8072.13808835, 0),
                new UnitVector3D(80.92876875, -58.74124946, 0),
                new UnitVector3D(58.74124946, 80.92876875, 0),
                new UnitVector3D(0, 0, 1)
                ));

            //lfy
            Lf.Add(500);
            Lh.Add(900);
            Bf.Add(1000);

            //Ekstremalne wartosc
            List<double> ExtremeXY = new List<double>();
            ExtremeXY.Add(4212.76031992); // max x
            ExtremeXY.Add(3951.29490255); // max y
            ExtremeXY.Add(-2298.14091626); // min x
            ExtremeXY.Add(0); //min y

            ExtremeXYs.Add(ExtremeXY);

            //transformed area results
            Dictionary<Areas, List<Point3D>> res1 = new Dictionary<Areas, List<Point3D>>();
            List<Point3D> F1transfResults = new List<Point3D>();
            F1transfResults.Add(new Point3D(-2298.14091626, 0, 0));
            F1transfResults.Add(new Point3D(1000.00000000, 0, 0));
            F1transfResults.Add(new Point3D(1000.00000000, 500.00000000, 0));
            F1transfResults.Add(new Point3D(-2298.14091626, 500.00000000, 0));
            res1.Add(Areas.F1, F1transfResults);

            List<Point3D> F2transResults = new List<Point3D>();
            F2transResults.Add(new Point3D(3212.76031992, 0, 0));
            F2transResults.Add(new Point3D(4212.76031992, 0, 0));
            F2transResults.Add(new Point3D(4212.76031992, 500.00000000, 0));
            F2transResults.Add(new Point3D(3212.76031992, 500.00000000, 0));
            res1.Add(Areas.F2, F2transResults);

            List<Point3D> GtransRes = new List<Point3D>();
            GtransRes.Add(new Point3D(1000.00000000, 0, 0));
            GtransRes.Add(new Point3D(3212.76031992, 0, 0));
            GtransRes.Add(new Point3D(3212.76031992, 500.00000000, 0));
            GtransRes.Add(new Point3D(1000.00000000, 500.00000000, 0));
            res1.Add(Areas.G, GtransRes);

            List<Point3D> HtransRes = new List<Point3D>();
            HtransRes.Add(new Point3D(-2298.14091626, 500.00000000, 0));
            HtransRes.Add(new Point3D(4212.76031992, 500.00000000, 0));
            HtransRes.Add(new Point3D(4212.76031992, 1400.00000000, 0));
            HtransRes.Add(new Point3D(-2298.14091626, 1400.00000000, 0));
            res1.Add(Areas.H, HtransRes);

            List<Point3D> ItransRes = new List<Point3D>();
            ItransRes.Add(new Point3D(-2298.14091626, 1400.00000000, 0));
            ItransRes.Add(new Point3D(4212.76031992, 1400.00000000, 0));
            ItransRes.Add(new Point3D(4212.76031992, 3951.29490255, 0));
            ItransRes.Add(new Point3D(-2298.14091626, 3951.29490255, 0));
            res1.Add(Areas.I, ItransRes);

            TransformedAreaResults.Add(res1);

            //arearesults
            Dictionary<Areas, List<Point3D>> AreaRes1 = new Dictionary<Areas, List<Point3D>>()
            {
                {Areas.F1, new List<Point3D>()
                    {
                        new Point3D(4321.91582786, 9422.09477686, 0),
                        new Point3D(6991.06066318, 7484.72559377, 0),
                        new Point3D(7284.76691047, 7889.36943754, 0),
                        new Point3D(4615.62207515, 9826.73862063, 0)
                    }
                },
                {
                    Areas.F2, new List<Point3D>()
                    {
                        new Point3D(8781.82034558, 6184.92253434, 0),
                        new Point3D(9591.10803313, 5597.51003976, 0),
                        new Point3D(9884.81428042, 6002.15388353, 0),
                        new Point3D(9075.52659287, 6589.56637811, 0)
                    }
                },
                {
                    Areas.G, new List<Point3D>()
                    {
                        new Point3D(6991.06066318, 7484.72559377, 0),
                        new Point3D(8781.82034558, 6184.92253434, 0),
                        new Point3D(9075.52659287, 6589.56637811, 0),
                        new Point3D(7284.76691047, 7889.36943754, 0)
                    }
                },
                {
                    Areas.H, new List<Point3D>()
                    {
                        new Point3D(4615.62207515, 9826.73862063, 0),
                        new Point3D(9884.81428042, 6002.15388353, 0),
                        new Point3D(10413.48552554, 6730.51280232, 0),
                        new Point3D(5144.29332027, 10555.09753943, 0)
                    }
                },
                {
                    Areas.I, new List<Point3D>()
                    {
                        new Point3D(5144.29332027, 10555.09753943, 0),
                        new Point3D(10413.48552554, 6730.51280232, 0),
                        new Point3D(11912.14802865, 8795.24435425, 0),
                        new Point3D(6642.95582338, 12619.82909136, 0)
                    }
                }
            };
            AreaResults.Add(AreaRes1);
            #endregion

            //dodanie wszystkich uzupelnionych list do kontenerow
            lists.Add(DemoRects);
            lists.Add(TransformResults);
            lists.Add(TransformedCS);
            lists.Add(Lf);
            lists.Add(Lh);
            lists.Add(Bf);
            lists.Add(LGs);
            lists.Add(ExtremeXYs);
            lists.Add(TransformedAreaResults);
            lists.Add(AreaResults);

            for (int i = 0; i < DemoRects.Count; i++)
            {
                LoadGeometry LG = new LoadGeometry(DemoRects[i]);
                LG.Bf = Bf[i];
                LG.Lf = Lf[i];
                LG.Lh = Lh[i];
                LGs.Add(LG);
            }

            //foreach (List<Point3D> DemoRect in DemoRects)
            //{
            //    LoadGeometry LG = new LoadGeometry(DemoRect);
            //    LG
            //    LGs.Add(LG);
            //}
        }

        [TestMethod]
        public void CheckTestDataConsistency()
        {
            IEnumerable<int> counts = (from a in lists select a.Count).Distinct();
            Assert.AreEqual(counts.Count(), 1);
        }

        [TestMethod]
        public void BuildCsTest()
        {
            Initialize();
            List<List<Point3D>>.Enumerator eDemoRects = DemoRects.GetEnumerator();
            List<CoordinateSystem>.Enumerator eDemoCS = TransformedCS.GetEnumerator();

            while (eDemoRects.MoveNext() && eDemoCS.MoveNext())
            {
                List<Point3D> DemoRect = eDemoRects.Current;
                CoordinateSystem CS = eDemoCS.Current;
                CoordinateSystem myCS = RWL.BuildSystem(DemoRect[0], DemoRect);
                for (int i = 0; i < CS.Values.Length; i++)
                {
                    double diff = Math.Abs(CS.Values[i] - myCS.Values[i]);
                    Assert.IsTrue(RWL.FuzzyCompare(CS.Values[i], myCS.Values[i]));
                }
            }
        }

        [TestMethod]
        public void FuzzyCompareTest()
        {
            Assert.IsTrue(RWL.FuzzyCompare(0.000001, 0.0000005));
            Assert.IsFalse(RWL.FuzzyCompare(0.000001, 0.000002));
        }

        [TestMethod]
        public void TransformTests()
        {
            Initialize();
            List<List<Point3D>>.Enumerator eDemoRects = DemoRects.GetEnumerator();
            List<List<Point3D>>.Enumerator eDemoResults = TransformResults.GetEnumerator();
            List<LoadGeometry>.Enumerator eLG = LGs.GetEnumerator();

            while (eDemoRects.MoveNext() && eDemoResults.MoveNext() && eLG.MoveNext())
            {
                List<Point3D> DemoRect = eDemoRects.Current;
                List<Point3D> DemoRes = eDemoResults.Current;
                LoadGeometry LG = eLG.Current;

                List<Point3D> results = LG.TransformedPolygon;

                for (int i = 0; i < results.Count; i++)
                {
                    Assert.IsTrue(RWL.FuzzyCompare(results[i].X, DemoRes[i].X));
                    Assert.IsTrue(RWL.FuzzyCompare(results[i].Y, DemoRes[i].Y));
                    Assert.IsTrue(RWL.FuzzyCompare(results[i].Z, DemoRes[i].Z));
                }
            }   
        }

        [TestMethod]
        public void FindExtremesXY()
        {
            List<LoadGeometry>.Enumerator eLG = LGs.GetEnumerator();
            List<List<double>>.Enumerator eEx = ExtremeXYs.GetEnumerator();
            while (eLG.MoveNext() && eEx.MoveNext())
            {
                LoadGeometry LG = eLG.Current;
                List<double> rEx = eEx.Current;
                Assert.IsTrue(RWL.FuzzyCompare(LG.MaxX, rEx[0]));
                Assert.IsTrue(RWL.FuzzyCompare(LG.MaxY, rEx[1]));
                Assert.IsTrue(RWL.FuzzyCompare(LG.MinX, rEx[2]));
                Assert.IsTrue(RWL.FuzzyCompare(LG.MinY, rEx[3]));

            }
        }

        [TestMethod]
        public void TestAreaVerticesAfterTransform()
        {
            List<LoadGeometry>.Enumerator eLG = LGs.GetEnumerator();
            List<Dictionary<Areas, List<Point3D>>>.Enumerator eRes = AreaResults.GetEnumerator();

            while (eRes.MoveNext() && eLG.MoveNext())
            {
                LoadGeometry LG = eLG.Current;
                Dictionary<Areas, List<Point3D>> results = eRes.Current;

                foreach (Areas area in Enum.GetValues(typeof(Areas)))
                {
                    List<Point3D> demoRes = results[area];
                    List<Point3D> myRes = LG.GetPointsInGCS(area);

                    Assert.AreEqual(demoRes.Count, myRes.Count);

                    for (int i = 0; i < demoRes.Count; i++)
                    {
                        Assert.IsTrue(RWL.FuzzyCompare(demoRes[i].X, myRes[i].X));
                        Assert.IsTrue(RWL.FuzzyCompare(demoRes[i].Y, myRes[i].Y));
                        Assert.IsTrue(RWL.FuzzyCompare(demoRes[i].Z, myRes[i].Z));
                    }
                }
            }
        }

        [TestMethod]
        public void TestAreaVerticesBeforeTransform()
        {
            List<LoadGeometry>.Enumerator eLG = LGs.GetEnumerator();
            List<Dictionary<Areas, List<Point3D>>>.Enumerator eRes = TransformedAreaResults.GetEnumerator();

            while (eRes.MoveNext() && eLG.MoveNext())
            {
                LoadGeometry LG = eLG.Current;
                Dictionary<Areas, List<Point3D>> results = eRes.Current;

                foreach (Areas area in Enum.GetValues(typeof(Areas)))
                {
                    List<Point3D> demoRes = results[area];
                    List<Point3D> myRes = LG.GetPointsInLCS(area);

                    Assert.AreEqual(demoRes.Count, myRes.Count);

                    for (int i = 0; i < demoRes.Count; i++)
                    {
                        Assert.IsTrue(RWL.FuzzyCompare(demoRes[i].X, myRes[i].X));
                        Assert.IsTrue(RWL.FuzzyCompare(demoRes[i].Y, myRes[i].Y));
                        Assert.IsTrue(RWL.FuzzyCompare(demoRes[i].Z, myRes[i].Z));
                    }
                }
            }
        }
    }
}
