﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RobotOM;

namespace ZShared.RobotHelpers
{
    public static class CaseHelpers
    {
        public static IRobotCase GetCurrentRobotCase()
        {
            RobotSelection sel = RobotHelper.GetSelection(IRobotObjectType.I_OT_CASE);
            int n = sel.Get(0);

            return RobotHelper.RStructure.Cases.Get(n);
        }

        public static IRobotSimpleCase SimpleCaseFromNumber(int no)
        {
            return CaseHelpers.CaseFromNumber(no) as IRobotSimpleCase;
        }

        public static IRobotCase CaseFromNumber(int no)
        {
            return RobotHelper.RStructure.Cases.Get(no);
        }
    }
}
