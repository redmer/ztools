﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MathNet.Spatial.Euclidean;
using RobotOM;
using ZShared.Helpers;


namespace ZShared.RobotHelpers
{
    public static class RobotHelper
    {
        private static RobotApplication _rapp = null;

        public static void Connect(RobotApplication rapp)
        {
            RobotHelper._rapp = rapp;
        }

        public static RobotApplication RApp
        {
            get
            {
                if (_rapp == null)
                    _rapp = new RobotApplication();
                if (_rapp.Visible == 0)
                    throw (new InvalidOperationException("Błąd podczas połączenia z robotem"));
                return _rapp;
            }
        }

        public static RobotStructure RStructure
        {
            get
            {

                return RobotHelper.RApp.Project.Structure;
            }
        }

        public static RobotSelection GetSelection(IRobotObjectType objType)
        {
            return RobotHelper.RStructure.Selections.Get(objType);
        }

        public static List<Point3D> PathFromCladding(RobotObjObject cladding)
        {
            List<Point3D> results = new List<Point3D>();
            for (int i = 1; i <= cladding.Main.DefPoints.Count; i++)
            {
                RobotGeoPoint3D pt = cladding.Main.DefPoints.Get(i);
                results.Add(new Point3D(pt.X, pt.Y, pt.Z));
            }
            return results;
        }

        public static Plane PlaneFromCladding(RobotObjObject cladding)
        {
            IRobotCollection col = cladding.Main.DefPoints;
            if (col.Count < 3)
                throw (new InvalidOperationException("Próba stworzenia płaszczyzny z mniej niż 2 punktów"));
            Point3D[] pts = new Point3D[3];

            for (int i = 1; i < col.Count + 1; i++)
            {
                RobotGeoPoint3D pt = col.Get(i);
                Point3D p = new Point3D(pt.X, pt.Y, pt.Z);
                if (i < 3)
                {
                    pts[i - 1] = p;
                    continue;
                }
                if (GeometryHelpers.CheckColinear(pts[0], pts[1], p))
                    continue;
                else
                {
                    pts[2] = p;
                    break;
                }
                throw new InvalidOperationException(string.Format("Nie znalazlem w panelu o numerze {0},3 niewspółliniowych punktów do utworzenia płaszczyzny", cladding.Number));
            }
            return new Plane(pts[0], pts[1], pts[2]);
        }
    }
}
