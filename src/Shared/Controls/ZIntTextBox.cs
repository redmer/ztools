﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZShared.Kernel;

namespace ZShared.Controls
{
    public delegate void IntValueChangedEventHandler(object source, IntEventArgs e);

    public class IntEventArgs : EventArgs
    {
        private int _newValue;

        public int NewValue {
            get
            {
                return _newValue;
            }
        }

        public IntEventArgs(int newValue)
        {
            _newValue = newValue;
        }
    }

    public partial class ZIntTextBox : UserControl
    {
        public event IntValueChangedEventHandler OnValueChanged;

        public TextBox NativeTextBox
        {
            get
            {
                return this.textBox1;
            }
        }

        Settings _settings;
        public int Value
        {
            get
            {
                int val = int.Parse(textBox1.Text);
                return val;
            }
            set
            {
                    textBox1.Text = value.ToString();
            }
        }

        public ZIntTextBox()
        {
            InitializeComponent();
            _settings = Settings.Instance;
            this.Value = 0;
            //this.LostFocus += ZIntTextBox_LostFocus;
            this.textBox1.LostFocus += ZIntTextBox_LostFocus;
        }

        private void ZIntTextBox_LostFocus(object sender, EventArgs e)
        {
            if (OnValueChanged != null)
            {
                OnValueChanged(this, new IntEventArgs(Value));
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string t = textBox1.Text;
            int d = 0;
            if (!int.TryParse(t, out d))
            {
                textBox1.BackColor = _settings.GetColor("Colors.ValidationError");
            }
            else
            {
                textBox1.BackColor = _settings.GetColor("Colors.ValidationOk");
            }
        }
    }
}
