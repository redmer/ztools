﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ZShared.RobotHelpers;
using RobotOM;

namespace ZShared.Controls
{
    public partial class RobotCaseControl : UserControl
    {
        RobotStructure RS;
        RobotCaseServer RC;

        public int LoadCaseNo
        {
            get
            {
                return int.Parse(comboBox1.Text);
            }
        }

        public RobotCaseControl()
        {
            InitializeComponent();
            Load += RobotCaseControl_Load;
            comboBox1.LostFocus += ComboBox1_LostFocus;
            comboBox1.SelectedValueChanged += ComboBox1_SelectedValueChanged;
        }

        private void ComboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            int newNumber = int.Parse(comboBox1.Text);
            IRobotCase cc = RC.Get(newNumber);
            if (cc != null)
                textBox1.Text = cc.Name;
            else
                MessageBox.Show("Podany przypadek nie istnieje");
        }

        private void ComboBox1_LostFocus(object sender, EventArgs e)
        {
            int newNumber = int.Parse(comboBox1.Text);
            IRobotCase cc = RC.Get(newNumber);
            if (cc != null)
                textBox1.Text = cc.Name;
            else
                MessageBox.Show("Podany przypadek nie istnieje");
        }

        private void RobotCaseControl_Load(object sender, EventArgs e)
        {
            buildCaseList();
            IRobotCase cs = CaseHelpers.GetCurrentRobotCase();
            if (cs != null)
                comboBox1.SelectedValue = cs.Number;
        }

        private void buildCaseList()
        {
            RS = RobotHelper.RStructure;
            RC = RS.Cases;
            List<IRobotCase> tempList = new List<IRobotCase>();
            RobotCaseCollection col = RC.GetAll();
            for (int i = 1; i < col.Count + 1; i++)
            {
                IRobotCase cs = col.Get(i);
                comboBox1.Items.Add(cs.Number);
            }
        }
    }
}
