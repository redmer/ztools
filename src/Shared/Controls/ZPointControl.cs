﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ZShared.RobotHelpers;
using MathNet.Spatial.Euclidean;
using RobotOM;

namespace ZShared.Controls
{
    public partial class ZPointControl : UserControl
    {
        public double X
        {
            get
            {
                return zDoubleTextBox1.Value;
            }
            set
            {
                zDoubleTextBox1.Value = value;
            }
        }

        public double Y
        {
            get
            {
                return zDoubleTextBox2.Value;
            }
            set
            {
                zDoubleTextBox2.Value = value;
            }
        }

        public double Z
        {
            get
            {
                return zDoubleTextBox3.Value;
            }
            set
            {
                zDoubleTextBox3.Value = value;
            }
        }

        public int NodeNumber
        {
            get
            {
                return this.zIntTextBox1.Value;
            }
            set
            {
                this.zIntTextBox1.Value = value;
            }
        }

        public Point3D GetPoint3D()
        {
            return new Point3D(X, Y, Z);
        }

        public double[] GetPointData()
        {
            double[] ret = new double[3];
            ret[0] = X;
            ret[1] = Y;
            ret[2] = Z;
            return ret;
        }

        protected TableLayoutPanel TablePanel
        {
            get
            {
                return tableLayoutPanel1;
            }
        }

        public ZPointControl()
        {
            InitializeComponent();
            this.zIntTextBox1.OnValueChanged += NodeNumberChanged;
        }

        private void NodeNumberChanged(object source, IntEventArgs e)
        {
            int val = e.NewValue;
            RobotStructure rs = RobotHelper.RStructure;
            RobotNode node;

            if (rs.Nodes.Exist(val) == 0)
                return;

            node = rs.Nodes.Get(val) as RobotNode;
            if (node == null)
            {
                throw new InvalidOperationException("Nie znalazłem węzła o tym numerze...");
            }
            //IRobotNode node = robj as IRobotNode;
            X = node.X;
            Y = node.Y;
            Z = node.Z;
        }

        public ZPointControl(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            RobotStructure rs = RobotHelper.RStructure;
            RobotSelection sel = rs.Selections.Get(IRobotObjectType.I_OT_NODE);
            if (sel.Count != 1)
            {
                MessageBox.Show("Wybierz dokładnie jeden węzeł");
                return;
            }

            int nodeNo = sel.Get(0);
            RobotNode node = rs.Nodes.Get(nodeNo) as RobotNode;
            NodeNumber = nodeNo;
            X = node.X;
            Y = node.Y;
            Z = node.Z;
        }
    }


}
