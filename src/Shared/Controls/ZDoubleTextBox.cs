﻿using System;
using System.Windows.Forms;

using ZShared.Kernel;

namespace ZShared.Controls
{
    public partial class ZDoubleTextBox : UserControl
    {
        public TextBox NativeTextBox
        {
            get
            {
                return textBox1;
            }
        }
        public double Value
        {
            get
            {
                return System.Convert.ToDouble(textBox1.Text);
            }
            set
            {
                textBox1.Text = value.ToString();
            }
        }

        private Settings _settings;

        public ZDoubleTextBox()
        {
            InitializeComponent();
            _settings = Settings.Instance;
         //   NativeTextBox.LostFocus += this.
            textBox1.TextChanged += TextBox1_TextChanged;
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            string t = textBox1.Text;
            double d = 0;
            if (!double.TryParse(t, out d))
            {
                textBox1.BackColor = _settings.GetColor("Colors.ValidationError");
            }
            else
            {
                textBox1.BackColor = _settings.GetColor("Colors.ValidationOk");
            }
        }
    }
}
