﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ZShared.Controls
{
    public partial class ZPointList : UserControl, ICollection<ZPointControl>
    {
        public BindingList<ZPointControl> ZPointControlList { get; }
        public ZPointControl this[int index]
        {
            get
            {
                return ZPointControlList[index];
            }
        }
        public String GroupBoxText
        {
            get
            {
                return groupBox1.Text;
            }
            set
            {
                groupBox1.Text = value;
            }
        }


        public ZPointList()
        {
            InitializeComponent();
            ZPointControlList = new BindingList<ZPointControl>();
            ZPointControlList.ListChanged += ZPointControlList_ListChanged;
        }

        private void ZPointControlList_ListChanged(object sender, ListChangedEventArgs e)
        {
            panel1.SuspendLayout();
            switch (e.ListChangedType)
            {
                case ListChangedType.ItemAdded: ItemHasBeenAdded(e.NewIndex);
                    break;
                case ListChangedType.ItemDeleted: ItemHasBeenDeleted(e.NewIndex);
                    break;
                default:
                    throw new NotImplementedException("Operacja niezaimplementowana");
            }
            panel1.ResumeLayout();
            
        }

        private void ItemHasBeenDeleted(int idx)
        {
            panel1.Controls.RemoveAt(idx);
        }

        private void ItemHasBeenAdded(int idx)
        {
            ZPointControl control = ZPointControlList[idx];
            control.Width = panel1.Width - panel1.Padding.Left - panel1.Padding.Right - control.Margin.Left - control.Margin.Right;
            panel1.Controls.Add(control);
            if (panel1.VerticalScroll.Visible == true)
            {
                int width = panel1.Width - panel1.Padding.Left - panel1.Padding.Right - control.Margin.Left - control.Margin.Right - 20;
                foreach (Control c in panel1.Controls)
                {
                    c.Width = width;
                }
            }
            panel1.ResumeLayout();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            ZPointListControl c = new ZPointListControl();
            c.OnDeleteButtonClicked += DeleteButton_Click;
            ZPointControlList.Add(c);
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            ZPointListControl c = sender as ZPointListControl;
            if (c == null)
                throw new InvalidOperationException("Kontrolka nie odpowiada za punkt, nigdy nie powinna się tu znaleźć");
            ZPointControlList.Remove(c);
        }

        #region ICollection
        public void Add(ZPointControl item)
        {
            ZPointControlList.Add(item);
        }

        public void Clear()
        {
            ZPointControlList.Clear();
        }

        public bool Contains(ZPointControl item)
        {
            return ZPointControlList.Contains(item);
        }

        public void CopyTo(ZPointControl[] array, int arrayIndex)
        {
            ZPointControlList.CopyTo(array, arrayIndex);
        }

        public bool Remove(ZPointControl item)
        {
            return ZPointControlList.Remove(item);
        }

        public IEnumerator<ZPointControl> GetEnumerator()
        {
            return ZPointControlList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ZPointControlList.GetEnumerator();
        }

        public int Count
        {
            get
            {
                return ZPointControlList.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                //to musi byc true dopoki nie bedzie mechanizmu ogladania listy
                return ZPointControlList.AllowEdit && ZPointControlList.AllowNew && ZPointControlList.AllowRemove;
            }
        }
        #endregion

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
