﻿namespace ZShared.Controls
{
    partial class ZPointControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.zDoubleTextBox2 = new ZShared.Controls.ZDoubleTextBox();
            this.zDoubleTextBox3 = new ZShared.Controls.ZDoubleTextBox();
            this.zDoubleTextBox1 = new ZShared.Controls.ZDoubleTextBox();
            this.zIntTextBox1 = new ZShared.Controls.ZIntTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 9;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.zDoubleTextBox2, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.zDoubleTextBox3, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.zDoubleTextBox1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.zIntTextBox1, 7, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(460, 26);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(9, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(109, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(9, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Y";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(215, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(9, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Z";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(401, 1);
            this.button1.Margin = new System.Windows.Forms.Padding(1);
            this.button1.MinimumSize = new System.Drawing.Size(40, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 24);
            this.button1.TabIndex = 3;
            this.button1.Text = "Pobierz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(321, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 26);
            this.label4.TabIndex = 7;
            this.label4.Text = "NR";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // zDoubleTextBox2
            // 
            this.zDoubleTextBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zDoubleTextBox2.Location = new System.Drawing.Point(124, 3);
            this.zDoubleTextBox2.Name = "zDoubleTextBox2";
            this.zDoubleTextBox2.Size = new System.Drawing.Size(85, 20);
            this.zDoubleTextBox2.TabIndex = 5;
            this.zDoubleTextBox2.Value = 0D;
            // 
            // zDoubleTextBox3
            // 
            this.zDoubleTextBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zDoubleTextBox3.Location = new System.Drawing.Point(230, 3);
            this.zDoubleTextBox3.Name = "zDoubleTextBox3";
            this.zDoubleTextBox3.Size = new System.Drawing.Size(85, 20);
            this.zDoubleTextBox3.TabIndex = 6;
            this.zDoubleTextBox3.Value = 0D;
            // 
            // zDoubleTextBox1
            // 
            this.zDoubleTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zDoubleTextBox1.Location = new System.Drawing.Point(18, 3);
            this.zDoubleTextBox1.Name = "zDoubleTextBox1";
            this.zDoubleTextBox1.Size = new System.Drawing.Size(85, 20);
            this.zDoubleTextBox1.TabIndex = 4;
            this.zDoubleTextBox1.Value = 0D;
            // 
            // zIntTextBox1
            // 
            this.zIntTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zIntTextBox1.Location = new System.Drawing.Point(351, 3);
            this.zIntTextBox1.MaximumSize = new System.Drawing.Size(100, 25);
            this.zIntTextBox1.MinimumSize = new System.Drawing.Size(10, 20);
            this.zIntTextBox1.Name = "zIntTextBox1";
            this.zIntTextBox1.Size = new System.Drawing.Size(46, 20);
            this.zIntTextBox1.TabIndex = 8;
            this.zIntTextBox1.Value = 0;
            // 
            // ZPointControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(8000, 30);
            this.MinimumSize = new System.Drawing.Size(175, 15);
            this.Name = "ZPointControl";
            this.Size = new System.Drawing.Size(460, 26);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private ZDoubleTextBox zDoubleTextBox1;
        private ZDoubleTextBox zDoubleTextBox2;
        private ZDoubleTextBox zDoubleTextBox3;
        private System.Windows.Forms.Label label4;
        private ZIntTextBox zIntTextBox1;
    }
}
