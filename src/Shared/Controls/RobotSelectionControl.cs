﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using RobotOM;

namespace ZShared.Controls
{
    public partial class RobotSelectionControl : UserControl
    {
        private RobotApplication _rapp = null;
        private RobotApplication rapp
        {
            get
            {
                if (_rapp == null)
                    _rapp = RobotHelpers.RobotHelper.RApp;
                return _rapp;
            }
        }

        public IRobotObjectType ObjectType { get; set; }
        public String Selection
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                this.textBox1.Text = value;
            }
        }


        public RobotSelection GetRobotSelection()
        {
            
            RobotSelection sel = rapp.Project.Structure.Selections.Create(ObjectType);
            sel.AddText(Selection);
            return sel;
        }

        public void SetRobotSelection(RobotSelection sel)
        {
            textBox1.Text = sel.ToText();
        }

        public RobotSelectionControl()
        {
            InitializeComponent();
            ObjectType = IRobotObjectType.I_OT_BAR;
            _rapp = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RobotSelection sel = rapp.Project.Structure.Selections.Get(ObjectType);
            textBox1.Text = sel.ToText();
        }
    }
}
