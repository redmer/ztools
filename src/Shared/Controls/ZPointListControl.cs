﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZShared.Controls
{
    public delegate void DeleteButtonClickedEventHandler(object source, EventArgs e);

    public class ZPointListControl : ZPointControl
    {
        public event DeleteButtonClickedEventHandler OnDeleteButtonClicked;
        public Button DeleteButton { get; set; }

        public ZPointListControl() : base()
        {
            AddDeleteButton();
        }

        public ZPointListControl(double x, double y, double z) : base(x, y, z)
        {
            AddDeleteButton();
        }

        protected void AddDeleteButton()
        {
            Panel p = new Panel();
            DeleteButton = new Button();
            DeleteButton.Text = "Usuń";
            DeleteButton.Dock = DockStyle.None;
            DeleteButton.Size = new System.Drawing.Size(58, 24);
            DeleteButton.MaximumSize = new System.Drawing.Size(58, 24);
            DeleteButton.Click += DeleteButton_Click;

            p.Padding = new Padding(0, 4, 0, 0);
            p.Dock = DockStyle.Right;
            //p.BorderStyle = BorderStyle.FixedSingle;
            p.AutoSize = true;
            p.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            p.Controls.Add(DeleteButton);
            Controls.Add(p);
            DeleteButton.Top =  1;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (OnDeleteButtonClicked != null)
            {
                OnDeleteButtonClicked(this, e);
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ZPointListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "ZPointListControl";
            this.ResumeLayout(false);

        }
    }
}
