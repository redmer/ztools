﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace ZShared.Kernel
{
    public interface IPlugin
    {
        event PluginError PluginError;

        string MenuName { get; }
        Form MainInterface { get; }
    }
}
