﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MathNet.Spatial.Euclidean;
using MathNet.Numerics.LinearAlgebra;

namespace ZShared.Kernel
{
    public static class Helpers
    {
        public static bool FuzzyCompare(double u, double v, double delta = 0.000001)
        {
            return Math.Abs(u - v) < delta;
        }

        public static bool FuzzyCompare(Point3D p1, Point3D p2, double delta = 0.000001)
        {
            double[] p1a = p1.ToArray();
            double[] p2a = p2.ToArray();

            for (int i = 0; i < 3; i++)
            {
                if (FuzzyCompare(p1a[i], p2a[i], delta) == false)
                    return false;
            }
            return true;
        }
         
        public static double[] ToArray(this Point3D p)
        {
            double[] ret = new double[3];
            ret[0] = p.X;
            ret[1] = p.Y;
            ret[2] = p.Z;
            return ret;
        }

        public static ClipperLib.IntPoint ToIntPoint(this Point3D p, double scale = 1000000)
        {
            ClipperLib.IntPoint ip = new ClipperLib.IntPoint(p.X * scale, p.Y * scale);
            return ip;
        }

        public static List<ClipperLib.IntPoint> ToIntPointList(this List<Point3D> lst, double scale = 1000000)
        {
            List<ClipperLib.IntPoint> list = new List<ClipperLib.IntPoint>();
            foreach (Point3D p in lst)
            {
                list.Add(p.ToIntPoint(scale));
            }
            return list;
        }

        public static List<Point3D> ToPoint3DList(this List<ClipperLib.IntPoint> lst, double scale = 1000000)
        {
            List<Point3D> newlst = new List<Point3D>();
            foreach (ClipperLib.IntPoint ip in lst)
            {
                newlst.Add(new Point3D((double)ip.X / scale, (double)ip.Y / scale, 0));
            }
            return newlst;
        }

        public static List<Point3D> UnTransformPoints(this ZShared.Helpers.Transformation TR, List<Point3D> points)
        {
            List<Point3D> results = new List<Point3D>();
            foreach (Point3D p in points)
            {
                results.Add(new Point3D(TR.UnTransform(p.ToArray())));
            }
            return results;
        }

        public static List<Point3D> TransformPoints(this ZShared.Helpers.Transformation TR, List<Point3D> points)
        {
            List<Point3D> results = new List<Point3D>();
            foreach (Point3D p in points)
            {
                results.Add(new Point3D(TR.TransformPoint(p.ToArray())));
            }
            return results;
        }

        // punkty 3d w tym miejscu sa tylko dlatego zeby latwiej bylo operowac w 
        // przestrzeni 3d, w rzeczywistosci podczas konwersji do IntPoint wartosc
        // z jest ignorowana i nie bedzie mozna jej odtworzyc, glownym ograniczeniem
        // w tym miejscu jest biblioteka clipper lib w razie czego mozna 
        // ta metode przepisac, metoda zle dziala gdy poligon jest zdegenerowany
        public static bool ComparePolygons(this List<Point3D> l1, List<Point3D> l2, double scale = 1000000, double limit = 0.000001)
        {
            //wybierz jeden
            ClipperLib.Clipper clipper = new ClipperLib.Clipper();

            clipper.AddPath(l1.ToIntPointList(scale), ClipperLib.PolyType.ptSubject, true);
            clipper.AddPath(l2.ToIntPointList(scale), ClipperLib.PolyType.ptClip, true);

            List<List<ClipperLib.IntPoint>> results = new List<List<ClipperLib.IntPoint>>();

            clipper.Execute(ClipperLib.ClipType.ctDifference, results);

            if (results.Count == 0)
                return true;

            List<List<Point3D>> resultsP = new List<List<Point3D>>();
            foreach (List<ClipperLib.IntPoint> lst in results)
            {
                resultsP.Add(lst.ToPoint3DList(scale));
            }

            //sprawdzenie czy pole jest bardzo male
            double AreaSum = 0;
            foreach (List<Point3D> lst in resultsP)
            {
                AreaSum += AreaOfPolygon(lst);
            }

            //obliczam srednia wielkosc polygonu
            double Area = (l1.AreaOfPolygon() + l2.AreaOfPolygon()) / 2;

            return AreaSum < limit * Area;
        }

        public static double AreaOfPolygon(this List<Point3D> lst)
        {
            bool p;
            return AreaOfPolygon(lst, out p);
        }

        public static double AreaOfPolygon(this List<Point3D> lst, out bool counterclockwise)
        {
            double sumdet = 0;
            for (int i = 0; i< lst.Count; i++)
            {
                int n = i;
                int m = i == lst.Count - 1 ? 0 : i + 1;
                Point3D p1 = lst[n];
                Point3D p2 = lst[m];
                double det = p1.X * p2.Y - p2.X * p1.Y;
                sumdet += det;
            }
            counterclockwise = sumdet > 0 ? true : false;
            return Math.Abs(0.5 * sumdet);
        }
    }
}
