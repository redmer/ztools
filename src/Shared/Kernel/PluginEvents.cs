﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZShared.Kernel
{
    public class PluginErrorEventArgs : EventArgs
    {
        public string Message { get; set; }

        public PluginErrorEventArgs(string message)
        {
            Message = message;
        }
    }

    public delegate void PluginError(object sender, PluginErrorEventArgs args);

        
}
