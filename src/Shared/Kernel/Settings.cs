﻿using System.Collections.Generic;
using System.Drawing;

namespace ZShared.Kernel
{
    public class Settings
    {
        private Dictionary<string, object> _settings = new Dictionary<string, object>()
            {
                { "PluginsPath", "plugins" },
                {"MainWindow.Button.Size", new Size(70, 70) },
                {"Colors.ValidationError", Color.LightPink},
                {"Colors.ValidationOk", Color.LightGreen }
            };

        private static Settings _settingsInstance = null;

        public static Settings Instance
        {
            get
            {
                if (_settingsInstance != null)
                    return _settingsInstance;
                else
                {
                    _settingsInstance = new Settings();
                    return _settingsInstance;
                }
            }
        }

        private Settings()
        { }

        public object this[string k]
        {
            get
            {
                return _settings[k];
            }
            set
            {
                if (_settings.ContainsKey(k))
                    _settings[k] = value;
                else
                    _settings.Add(k, value);
            }
        }

        public Color GetColor(string key)
        {
            return (Color)_settings[key];
        }
    }
}
