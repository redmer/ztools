﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MathNet.Spatial.Euclidean;

namespace ZShared.Kernel
{
    public class ZSpace
    {
        public Plane Pl1;
        public Plane Pl2;

        private int pl1operator = 0;
        private int pl2operator = 0;

        public Point3D PointInSpace;

        public ZSpace(Plane pl1, Point3D p1, Plane pl2, Point3D p2)
        {
            if (pl1 == pl2)
                throw new InvalidOperationException("Płaszczyzny nie mogą być równe");
            if (pl1.Normal != pl2.Normal)
                throw new InvalidOperationException("Płaszczyzny nie są do siebie równoległe");

            if (PlaneEqRes(pl1, p1) > 0)
                pl1operator = 1;
            else
                pl1operator = -1;

            if (PlaneEqRes(pl2, p2) > 0)
                pl2operator = 1;
            else
                pl2operator = -1;

            Pl1 = pl1;
            Pl2 = pl2;
            
        }
        
        public bool InSpace(Point3D p)
        {
            double pl1res = (pl1operator * PlaneEqRes(Pl1, p));
            double pl2res = (pl2operator * PlaneEqRes(Pl2, p));

            if (pl1res < 0)
                return false;

            if (pl2res < 0)
                return false;

            return true;
        }

        private double PlaneEqRes(Plane pl, Point3D p)
        {
            return pl.A * p.X + pl.B * p.Y + pl.C * p.Z + pl.D;
        }
    }
}
