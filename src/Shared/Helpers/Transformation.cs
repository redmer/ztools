﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using MathNet.Numerics.LinearAlgebra;

namespace ZShared.Helpers
{
    public class Transformation : IDisposable
    {
        double[] u;
        double[] v;
        double[] w;

        // początek nowego ukladu wspołrzędnych
        List<double> p1;
        List<double> p2;
        List<double> p3;

        Matrix<double> TMatrix;
        Matrix<double> UTMatrix;
        Matrix<double> T;
        Matrix<double> R;

        /// <summary>
        /// Wyznacza macierz transformacji budując ją na podstawie trzech punktów
        /// w ten sposób, że os x' jest rownolegla do wektora u=[p2 - p1] os z
        /// wyznaczana jest jako prostopadla do wektora tmp1=[p3 - p1] następnie wyznaczana
        /// jest os y jako prostopadla do wyznaczonej wczesniej plaszczyzny,
        /// Uwaga w przypadku takiego zalozenia punktów, podawane punk
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        public Transformation(double[] p1, double[] p2, double[] p3)
        {
            u = new double[3];
            double[] tmp1 = new double[3];
            for (int i = 0; i < u.Length; i++)
            {
                u[i] = p2[i] - p1[i];
                tmp1[i] = p3[i] - p1[i];
            }

            u = Normalize(u);
            tmp1 = Normalize(tmp1);

            w = CrossProduct(u, tmp1);
            w = Normalize(w);
            v = CrossProduct(w, u);
            v = Normalize(v);

            this.p1 = p1.ToList<double>();
            this.p2 = p2.ToList<double>();
            this.p3 = p3.ToList<double>();

            TMatrix = Matrix<double>.Build.Dense(4, 4);
            T = Matrix<double>.Build.Dense(4, 4);
            R = Matrix<double>.Build.Dense(4, 4);

            BuildTranslationMatrix(p1, ref T);
            BuildRotationMatrix(u, v, w, ref R);

            TMatrix = R.Multiply(T);
            double dett = TMatrix.Determinant();
            UTMatrix = TMatrix.Inverse();
        }

        private double[] Normalize(double[] v)
        {
            if (v.Length != 3)
                throw new InvalidOperationException("Wektor musi miec 3 wspolrzedne");
            double[] normalized = new double[3];
            double len = LengthOfVector(v);
            for (int i = 0; i < v.Length; i++)
                normalized[i] = v[i] / len;
            return normalized;
        }

        private double LengthOfVector(double[] v)
        {
            if (v.Length != 3)
                throw new InvalidOperationException("Wektor musi miec 3 wspolrzedne");
            return Math.Sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
        }

        public double[] UnTransform(double[] pprim)
        {
            if (pprim.Length != 3)
                throw new InvalidOperationException("Punkt musi mieć 3 współrzędne");
            Vector<double> Pprim = Vector<double>.Build.Dense(4, 1);
            for (int i = 0; i < pprim.Length; i++)
                Pprim[i] = pprim[i];
            Vector<double> P = UTMatrix.Multiply(Pprim);
            double[] p = P.ToArray();
            Array.Resize<double>(ref p, 3);
            return p;
        }

        public double[] TransformPoint(double[] p)
        {
            if (p.Length != 3)
                throw new InvalidOperationException("Punkt musi mieć 3 współrzędne");
            Vector<double> P = Vector<double>.Build.Dense(4, 1);
            for (int i = 0; i < p.Length; i++)
                P[i] = p[i];
            Vector<double> PPrim = TMatrix.Multiply(P);
            double[] pprim = PPrim.ToArray();
            Array.Resize<double>(ref pprim, 3);
            return pprim;
        }

        protected void BuildTranslationMatrix(double[] p, ref Matrix<double> T)
        {
            T[0, 0] = 1;
            T[1, 0] = 0;
            T[2, 0] = 0;
            T[3, 0] = 0;

            T[0, 1] = 0;
            T[1, 1] = 1;
            T[2, 1] = 0;
            T[3, 1] = 0;

            T[0, 2] = 0;
            T[1, 2] = 0;
            T[2, 2] = 1;
            T[3, 2] = 0;

            T[0, 3] = -p[0];
            T[1, 3] = -p[1];
            T[2, 3] = -p[2];
            T[3, 3] = 1;
        }

        protected void BuildRotationMatrix(double[] u, double[] v, double[] w, ref Matrix<double> R)
        {
            //pierwsza kolumna
            R[0, 0] = u[0];
            R[1, 0] = v[0];
            R[2, 0] = w[0];
            R[3, 0] = 0;

            //druga kolumna
            R[0, 1] = u[1];
            R[1, 1] = v[1];
            R[2, 1] = w[1];
            R[3, 1] = 0;

            //trzecia kolumna
            R[0, 2] = u[2];
            R[1, 2] = v[2];
            R[2, 2] = w[2];
            R[3, 2] = 0;

            //trzecia kolumna
            R[0, 3] = 0;
            R[1, 3] = 0;
            R[2, 3] = 0;
            R[3, 3] = 1;
        }

        public static double[] CrossProduct(double[] u, double[] v)
        {
            double[] result = new double[3];
            result[0] = u[1] * v[2] - u[2] * v[1];
            result[1] = u[2] * v[0] - u[0] * v[2];
            result[2] = u[0] * v[1] - u[1] * v[0];
            return result;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    
                    // TODO: dispose managed state (managed objects).
                }

                p1 = null;
                p2 = null;
                p3 = null;
                TMatrix = null;
                UTMatrix = null;

                u = null;
                v = null;
                w = null;

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Transformation() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
