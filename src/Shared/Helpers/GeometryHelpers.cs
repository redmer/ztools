﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MathNet.Spatial.Euclidean;

namespace ZShared.Helpers
{
    public class GeometryHelpers
    {
        public static bool CheckColinear(Point3D p1, Point3D p2, Point3D p3, double tolerance = 1e-8)
        {
            Vector3D v1 = p2 - p1;
            Vector3D v2 = p3 - p1;
            return v1.IsParallelTo(v2, tolerance);
        }

        public static bool CheckColinear(Point3D[] points, double tolerance = 1e-8)
        {
            if (points.Length != 3)
                throw new InvalidOperationException("Tablica powinna składać się dokładnie z 3 punktów");
            return GeometryHelpers.CheckColinear(points[0], points[1], points[2], tolerance);
        }


        /// <summary>
        /// </summary>
        /// <param name="points">Lista punktów</param>
        /// <param name="tolerance">Tolerancja - domyslna daje dobre wyniki dla rezultatów okolo 3 miejsc po przecinku - patrz testy</param>
        /// <returns>Zwraca trzy pierwsze napotkanie niewspółliniowe punkty. Jeśli nie znajdzie zwraca 0</returns>
        public static List<Point3D> Find3NonColinearPoints(IEnumerable<Point3D> points, double tolerance = 1e-8)
        {
            List<Point3D> result = new List<Point3D>();
            IEnumerator<Point3D> ePoints = points.GetEnumerator();
            while (ePoints.MoveNext())
            {
                if (result.Count < 2)
                {
                    result.Add(ePoints.Current);
                    continue;
                }
                else
                {
                    if (CheckColinear(result[0], result[1], ePoints.Current, tolerance))
                        continue;
                    else
                    {
                        result.Add(ePoints.Current);
                        return result;
                    }
                }
            }
            return null;
        }
    }
}
