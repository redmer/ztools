﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ZShared.RobotHelpers;
using RobotOM;
using MathNet.Spatial.Euclidean;

namespace ZTools.PointDistance
{
    public partial class UI : Form
    {
        public UI()
        {
            InitializeComponent();
            this.TopMost = true;
            this.GotFocus += UI_GotFocus;
            this.Click += UI_Click;
            this.label1.Click += Label1_Click;
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            calculate();
        }

        private void calculate()
        {
            RobotStructure rstr = RobotHelper.RStructure;
            RobotSelection sel = rstr.Selections.Get(IRobotObjectType.I_OT_NODE);
            if (sel.Count == 2)
            {
                int nodeno1 = sel.Get(1);
                int nodeno2 = sel.Get(2);

                RobotNode n1 = rstr.Nodes.Get(nodeno1) as RobotNode;
                RobotNode n2 = rstr.Nodes.Get(nodeno2) as RobotNode;

                Point3D p1 = new Point3D(n1.X, n1.Y, n1.Z);
                Point3D p2 = new Point3D(n2.X, n2.Y, n2.Z);

                label1.Text = string.Format("{0} [m]", p1.DistanceTo(p2).ToString("F3"));
            }
        }

        private void UI_Click(object sender, EventArgs e)
        {
            calculate();
        }

        private void UI_GotFocus(object sender, EventArgs e)
        {
            calculate();
        }
    }
}
