﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ZShared.Kernel;

namespace ZTools.PointDistance
{
    public class PointDistance : IPlugin
    {
        private UI _ui;

        public PointDistance()
        {
            _ui = new UI();
        }

        public System.Windows.Forms.Form MainInterface
        {
            get
            {
                return _ui;
            }
        }

        public string MenuName
        {
            get
            {
                return "Odległość między dwoma punktami";
            }
        }

        public event PluginError PluginError;
    }
}
