ZTools for Robot Structural Analysis
====================================
Downloads
---------
[Wersja 0.0.1 - dla Robot Structural Analysis 2016](https://bitbucket.org/redmer/ztools/downloads/ztools_0.0.1.7z)

Short description
-----------------
Application is designed to be an addon for Autodesk Robot Structural Analysis,
that simplifies and improve some repetitve tasks. ZTools is built of two main
modules which is ZTools itself and shared lib.

# ZTools itself
Is small application that wrap all of the tools in one window, provides fast
access for the plugins written for ZTools

# SharedLib
Shared library is a lib which has interface for the plugin, and provides some 
more classes for faster writing new tools. It provides also some user controls
that are designed to work with RSA and with numerical data

# Plugins

## Pomysly na addony
1. Równowaga węzła
2. **URGENT** Kopiowanie obciążeń pomiędzy przypadkami
3. Kopiowanie obciążeń pomiędzy okładzinami
4. (DONE) ~~Filtr geometryczny
	https://forums.autodesk.com/t5/robot-structural-analysis/api-selecting-objects-by-orientation/td-p/5305313~~
5. Usuwanie z paneli/okładzin niepotrzebnych węzłów!
6. Mergowanie belek (http://forums.autodesk.com/t5/robot-structural-analysis/api-addin-for-merging-bars/td-p/3817730)
7. Sprawdzenie czy panele / okladziny są płaskie! 
8. Zamiana panelu na okładzinę
9. Zmieniacz barsów do linii
10. Generator widoków (po osiach)
11. Zamiana na trójkąty (http://forums.autodesk.com/t5/robot-structural-analysis/api-objects-geometry-to-triangles-converter/m-p/3897003/highlight/true#M13142)
12. **URGENT** Wykrywanie krótkich elementów obliczeniowych (PRIORYTET) http://forums.autodesk.com/t5/robot-structural-analysis/api-addin-for-short-calculation-elements-detection/td-p/3807860/highlight/true
13. Standardowe ustawienia projektu / a moze template?
14. Najkrótszy odcinek pomiędzy prętami (http://forums.autodesk.com/t5/robot-structural-analysis/api-addin-for-creating-clossest-nodes-between-bars/m-p/3871300/highlight/true#M12579)
15. Zaamieniacz krótkich prętów na połączenia sztywne
16. Kombinacje obciążeń robot (http://forums.autodesk.com/t5/robot-structural-analysis/editing-load-cases-in-excel-api/td-p/3320573/highlight/true/page/2)
17. Rozpoznanie które obciążenie nie zostało poprawnie rozłożone
    * Kopiuje każde z obciazenia do nowego przypadku
    * następnie usuwamy każdy kolejny przypadek aż zniknie to powiadomienie
    * (http://forums.autodesk.com/t5/robot-structural-analysis/linear-load-has-not-been-applied-to-the-panel/m-p/5539408#M31081)
18. Równowaga węzła
19. Zrzut otwartych tabel do excela (http://forums.autodesk.com/t5/robot-structural-analysis/api-macro-for-dumping-all-opened-tables-in-robot-with-all-tabs/td-p/5403391)
20. **URGENT** - autogrupowanie na podstawie (grupy normowe)
    * użytego profilu
    * użytego typu
    * użytych zwolnień
    * na podstawie wytężenia
21. Sprawdzenie czy wszystkie pręty są w grupie
22. Sprawdzenie czy pręty w grupach się powtarzają czy nie...

## WallWindLoad
### TODO
- [X] Sprowadzenie punktów konturu do płaszczyzny modelu panelów
- [X] Sprawdzenie czy zaznaczone panele leżą w jednej płaszczyźnie

## PointDistance

Plugin do obliczania odległości między węzłami

## LoadCaseLoadCopier

Plugin do kopiowania obciążeń pomiędzy przypadkami. Wspierane obciążenia zaznaczone poniżej:
```
#!csharp
    enum {
        I_LRT_NODE_FORCE = 0, //pierwsze wspierane obciazenie
        I_LRT_NODE_DISPLACEMENT = 1,
        I_LRT_BAR_DILATATION = 2,
        I_LRT_BAR_FORCE_CONCENTRATED = 3,
        I_LRT_BAR_MOMENT_DISTRIBUTED = 4,
        I_LRT_BAR_UNIFORM = 5,
        I_LRT_BAR_TRAPEZOIDALE = 6,
        I_LRT_BAR_DEAD = 7,
        I_LRT_DEAD = 7,
        I_LRT_BAR_THERMAL = 8,
        I_LRT_THERMAL = 8,
        I_LRT_NODE_VELOCITY = 10,
        I_LRT_NODE_ACCELERATION = 11,
        I_LRT_LINEAR_3D = 19,
        I_LRT_NODE_AUXILIARY = 20,
        I_LRT_LINEAR = 21,
        I_LRT_IN_3_POINTS = 22,
        I_LRT_POINT_AUXILIARY = 23,
        I_LRT_NODE_FORCE_IN_POINT = 23,
        I_LRT_PRESSURE = 24,
        I_LRT_THERMAL_IN_3_POINTS = 25,
        I_LRT_UNIFORM = 26,
        I_LRT_IN_CONTOUR = 28,
        I_LRT_NODE_FORCE_MASS = 30,
        I_LRT_BAR_FORCE_CONCENTRATED_MASS = 33,
        I_LRT_BAR_UNIFORM_MASS = 35,
        I_LRT_BAR_TRAPEZOIDALE_MASS = 36,
        I_LRT_MASS_ACTIVATION = 39,
        I_LRT_SPECTRUM_VALUE = 40,
        I_LRT_MOBILE_POINT_FORCE = 53,
        I_LRT_MOBILE_DISTRIBUTED = 55,
        I_LRT_LINEAR_ON_EDGES = 69,
        I_LRT_SURFACE_ON_OBJECT = 70
    }
```