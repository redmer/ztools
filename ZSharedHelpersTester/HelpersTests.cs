﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MathNet.Spatial.Euclidean;

using ZShared.Kernel;
using Helpers = ZShared.Kernel.Helpers;

namespace ZSharedTests
{
    [TestClass]
    public class HelpersTests
    {

        List<Point3D> l1 = new List<Point3D>
        {
            new Point3D(1.000001, 3, 0),
            new Point3D(2, 1, 0),
            new Point3D(3, 2, 0),
            new Point3D(4, 0, 0),
            new Point3D(5, 3, 0)
        };

        //to jest l1 z przesunietym wezlem (o 1)
        List<Point3D> l2 = new List<Point3D>
        {
            new Point3D(5, 3, 0),
            new Point3D(1, 3, 0),
            new Point3D(2, 1, 0),
            new Point3D(3, 2, 0),
            new Point3D(4, 0, 0)
        };

        //ta lista powinna sie roznic zarowno od l1 jak i l2
        List<Point3D> l3 = new List<Point3D>
        {
            new Point3D(0, 1, 0),
            new Point3D(5, 2, 0),
            new Point3D(0, 0, 0),
            new Point3D(0, 3, 0),
            new Point3D(1, 0, 0),
        };

        List<Point3D> l4 = new List<Point3D>
        {
            new Point3D(1.0000001, 3, 0),
            new Point3D(2, 1, 0),
            new Point3D(3, 2, 0),
            new Point3D(4, 0, 0),
            new Point3D(5, 3, 0)
        };

        [TestMethod]
        public void L1equalsL2()
        {
            Assert.IsTrue(Helpers.ComparePolygons(l1, l2));
        }

        [TestMethod]
        public void L1NotEqaulsL3()
        {
            Assert.IsFalse(Helpers.ComparePolygons(l1, l3));
        }

        [TestMethod]
        public void L2NotEqualsL3()
        {
            Assert.IsFalse(Helpers.ComparePolygons(l2, l3));
        }

        [TestMethod]
        public void L1EqualsL4WithChangedScale()
        {
            Assert.IsTrue(Helpers.ComparePolygons(l1, l4, 10000));
        }

        [TestMethod]
        public void TestAreaOfPolygon()
        {
            //537.50000000 tyle powinno wyjść
            List<Point3D> poly = new List<Point3D>()
            {
                new Point3D(0.00000000, 0.00000000, 0.00000000),
                new Point3D(25.00000000, 0.00000000, 0.00000000),
                new Point3D(40.00000000, 0.00000000, 0.00000000),
                new Point3D(50.00000000, 0.00000000, 0.00000000),
                new Point3D(50.00000000, 10.00000000, 0.00000000),
                new Point3D(40.00000000, 5.00000000, 0.00000000),
                new Point3D(25.00000000, 15.00000000, 0.00000000),
                new Point3D(0.00000000, 10.00000000, 0.00000000)
            };

            Assert.IsTrue(Helpers.FuzzyCompare(poly.AreaOfPolygon(), 537.500000));
        }

        [TestMethod]
        public void ListOfPointsTransformationa()
        {
            List<Point3D> AfterTransform = new List<Point3D>()
            {
                new Point3D(0.00000000, 0.00000000, 0.00000000),
                new Point3D(50.00000000, 0.00000000, 0.00000000),
                new Point3D(50.00000000, 11.03142601, 0.00000000),
                new Point3D(0.00000000, 11.03142601, 0.00000000)
            };

            List<Point3D> BeforeTransform = new List<Point3D>()
            {
                new Point3D(24.14814566, 6.47047613, 22.35320065),
                new Point3D(72.44443697, 19.41142838, 22.35320065),
                new Point3D(72.44443697, 19.41142838, 33.38462666),
                new Point3D(24.14814566, 6.47047613, 33.38462666)
            };

            using (ZShared.Helpers.Transformation TR = 
                new ZShared.Helpers.Transformation(
                    BeforeTransform[0].ToArray(), 
                    BeforeTransform[1].ToArray(), 
                    BeforeTransform[2].ToArray()))
            {
                List<Point3D> transformedPoints = TR.TransformPoints(BeforeTransform);
                Assert.IsTrue(transformedPoints.ComparePolygons(AfterTransform));
            }
        }

        [TestMethod]
        public void ListOfPointsUnTransformation()
        {
            List<Point3D> AfterTransform = new List<Point3D>()
            {
                new Point3D(0.00000000, 0.00000000, 0.00000000),
                new Point3D(50.00000000, 0.00000000, 0.00000000),
                new Point3D(50.00000000, 11.03142601, 0.00000000),
                new Point3D(0.00000000, 11.03142601, 0.00000000)
            };

            List<Point3D> BeforeTransform = new List<Point3D>()
            {
                new Point3D(24.14814566, 6.47047613, 22.35320065),
                new Point3D(72.44443697, 19.41142838, 22.35320065),
                new Point3D(72.44443697, 19.41142838, 33.38462666),
                new Point3D(24.14814566, 6.47047613, 33.38462666)
            };

            using (ZShared.Helpers.Transformation TR =
                    new ZShared.Helpers.Transformation(
                        BeforeTransform[0].ToArray(),
                        BeforeTransform[1].ToArray(),
                        BeforeTransform[2].ToArray()))
            {
                List<Point3D> UnTransformedPoints = TR.UnTransformPoints(AfterTransform);
                Assert.IsTrue(UnTransformedPoints.ComparePolygons(BeforeTransform));
            }
        }
    }
}
