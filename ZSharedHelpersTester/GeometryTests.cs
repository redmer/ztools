﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MathNet.Spatial.Euclidean;

using ZShared.Helpers;

namespace ZSharedHelpersTester
{
    [TestClass]
    public class GeometryTests
    {
        [TestMethod]
        public void CheckColinearShouldGiveTrue()
        {
            Point3D[] p = new Point3D[3]
            {
                new Point3D(4784.7512, 5444.0712, 702.6882),
                new Point3D(5229.8932, 6328.1915, 1081.9187),
                new Point3D(5681.1603, 7224.4772, 1466.3674)
            };
            Assert.IsTrue(GeometryHelpers.CheckColinear(p, 1e-8));
        }

        [TestMethod]
        public void CheckColinearShouldGiveFalse()
        {
            Point3D[] p = new Point3D[3]
            {
                new Point3D(4783.751, 5444.0712, 702.6882),
                new Point3D(5229.893, 6328.1915, 1081.9187),
                new Point3D(5681.160, 7224.4772, 1466.3674)
            };
            Assert.IsFalse(GeometryHelpers.CheckColinear(p, 1e-8));
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException),
         "A empty userId was inappropriately allowed.")]
        public void CheckColinearShouldGiveException()
        {
            Point3D[] p = new Point3D[3];
            GeometryHelpers.CheckColinear(p);
        }
    }
}
