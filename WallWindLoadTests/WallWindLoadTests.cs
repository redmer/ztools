﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Spatial.Euclidean;
using ZTools.WallWindLoad;
using ZShared.Kernel;


namespace WallWindLoadTests
{
    using h = ZShared.Kernel.Helpers;

    [TestClass]
    public class WallWindLoadTests
    {
        List<List<Point3D>> ProjectionPlanePointsInGCS;
        List<List<Point3D>> ProjectionPlanePointsInLCS;
        List<List<List<Point3D>>> PanelPointsInGCS;
        List<List<List<Point3D>>> PanelPointsInLCS;
        List<List<Point3D>> UserLoadPointsInLCS;
        List<List<Point3D>> UserLoadPointsInGCS;
        List<List<Point3D>> UserProjectedLoadPointsInLCS;
        List<List<List<Point3D>>> PanelsOutlineInLCS;
        List<List<Point3D>> LoadPolygonInLCS;

        List<List<Point3D>> ResultsInLCSA;
        List<List<Point3D>> ResultsInLCSB;
        List<List<Point3D>> ResultsInLCSC;

        List<List<Point3D>> Panel1AreaAResulst;

        List<WallWindLoadKernel> Kernels;

        [TestInitialize]
        public void DataInitialization()
        {
            #region ProjectionPlanePointsInGCS
            ProjectionPlanePointsInGCS = new List<List<Point3D>>()
            {
                new List<Point3D>()
                {
                    new Point3D(24.14814566, 6.47047613, 20.00000000),
                    new Point3D(72.44443697, 19.41142838, 20.00000000),
                }
            };
            #endregion
            #region ProjectionPlanePointsInLCS
            ProjectionPlanePointsInLCS = new List<List<Point3D>>()
            {
                new List<Point3D>()
                {
                    new Point3D(0.00000000, 0.00000000, 0.00000000),
                    new Point3D(50.00000000, 0.00000000, 0.00000000),
                }
            };
            #endregion
            #region PanelPointsInGCS
            PanelPointsInGCS = new List<List<List<Point3D>>>()
            {
                new List<List<Point3D>>()
                {
                        //panel 5
                        new List<Point3D>()
                        {
                            new Point3D(24.14814566, 6.47047613, 20.00000000),
                            new Point3D(47.51983418, 15.83872973, 20.00000000),
                            new Point3D(47.51983418, 15.83872973, 35.00000000),
                            new Point3D(24.14814566, 6.47047613, 30.00000000)
                        },
                        //panel 6
                        new List<Point3D>()
                        {
                            new Point3D(47.51983418, 15.83872973, 20.00000000),
                            new Point3D(63.30281680, 14.89138628, 20.00000000),
                            new Point3D(63.30281680, 14.89138628, 25.00000000),
                            new Point3D(47.51983418, 15.83872973, 35.00000000)
                        },
                        //panel 7
                        new List<Point3D>()
                        {
                            new Point3D(63.30281680, 14.89138628, 20.00000000),
                            new Point3D(72.44443697, 19.41142838, 20.00000000),
                            new Point3D(72.44443697, 19.41142838, 30.00000000),
                            new Point3D(63.30281680, 14.89138628, 25.00000000)
                        }
                    }
            };
#endregion
            #region PanelPointsInLCS
            PanelPointsInLCS = new List<List<List<Point3D>>>()
            {
                
                new List<List<Point3D>>()
                {
                    //panel5
                    new List<Point3D>()
                    {
                        new Point3D(0.00000000, 0.00000000, 0.00000000),
                        new Point3D(25.00000000, 0.00000000, -3.00000000),
                        new Point3D(25.00000000, 15.00000000, -3.00000000),
                        new Point3D(0.00000000, 10.00000000, 0.00000000)
                    },
                    //panel6
                    new List<Point3D>()
                    {
                        new Point3D(25.00000000, 0.00000000, -3.00000000),
                        new Point3D(40.00000000, 0.00000000, 2.00000000),
                        new Point3D(40.00000000, 5.00000000, 2.00000000),
                        new Point3D(25.00000000, 15.00000000, -3.00000000)
                    },
                    //panel7
                    new List<Point3D>()
                    {
                        new Point3D(40.00000000, 0.00000000, 2.00000000),
                        new Point3D(50.00000000, 0.00000000, 0.00000000),
                        new Point3D(50.00000000, 10.00000000, 0.00000000),
                        new Point3D(40.00000000, 5.00000000, 2.00000000)
                    }
                }
            };
            #endregion
            #region UserLoadPointsInLCS
            UserLoadPointsInLCS = new List<List<Point3D>>()
            {
                new List<Point3D>()
                {
                    new Point3D(0.00000000, 2.35320065, 0.00000000),
                    new Point3D(50.00000000, 2.35320065, 0.00000000),
                    new Point3D(47.11365029, 8.55682515, 0.57726994),
                    new Point3D(33.39081339, 9.40612441, -0.20306220),
                    new Point3D(16.92313329, 13.38462666, -2.03077600)
                }
            };
            #endregion
            #region UserLoadPointsInGCS
            UserLoadPointsInGCS = new List<List<Point3D>>()
            {
                new List<Point3D>()
                {
                    new Point3D(24.14814566, 6.47047613, 22.35320065),
                    new Point3D(72.44443697, 19.41142838, 22.35320065),
                    new Point3D(69.80584570, 18.10678616, 28.55682515),
                    new Point3D(56.34863831, 15.30879759, 29.40612441),
                    new Point3D(39.96903366, 12.81208431, 33.38462666)
                }
            };
            #endregion
            #region UserProjectedLoadPointsInLCS
            UserProjectedLoadPointsInLCS = new List<List<Point3D>>()
            {
                new List<Point3D>()
                {
                    new Point3D(0.00000000, 2.35320065, 0.00000000),
                    new Point3D(50.00000000, 2.35320065, 0.00000000),
                    new Point3D(47.11365029, 8.55682515, 0.00000000),
                    new Point3D(33.39081339, 9.40612441, 0.00000000),
                    new Point3D(16.92313329, 13.38462666, 0.00000000)
                }
            };
            #endregion
            #region PanelsOutlineInLCS
            PanelsOutlineInLCS = new List<List<List<Point3D>>>()
            {
                new List<List<Point3D>>()
                {
                    new List<Point3D>()
                    {
                    new Point3D(0.00000000, 0.00000000, 0.00000000),
                    new Point3D(50.00000000, 0.00000000, 0.00000000),
                    new Point3D(50.00000000, 10.00000000, 0.00000000),
                    new Point3D(40.00000000, 5.00000000, 0.00000000),
                    new Point3D(25.00000000, 15.00000000, 0.00000000),
                    new Point3D(0.00000000, 10.00000000, 0.00000000)
                    }
                }
            };
            #endregion
            #region ResultsInLCSA
            ResultsInLCSA = new List<List<Point3D>>()
            {
                new List<Point3D>()
                {
                    new Point3D(0.00000000, 2.35320065, 0.00000000),
                    new Point3D(6.00000000, 2.35320065, 0.00000000),
                    new Point3D(6.00000000, 13.38462666, 0.00000000),
                    new Point3D(0.00000000, 13.38462666, 0.00000000)
                }
            };
            #endregion
            #region ResultsInLCSB
            ResultsInLCSB = new List<List<Point3D>>()
            {
                new List<Point3D>()
                {
                    new Point3D(6.00000000, 2.35320065, 0.00000000),
                    new Point3D(18.00000000, 2.35320065, 0.00000000),
                    new Point3D(18.00000000, 13.38462666, 0.00000000),
                    new Point3D(6.00000000, 13.38462666, 0.00000000)
                }
            };
            #endregion
            #region ResutlsInLCSC
            ResultsInLCSC = new List<List<Point3D>>()
            {
                new List<Point3D>()
                {
                    new Point3D(18.00000000, 2.35320065, 0.00000000),
                    new Point3D(50.00000000, 2.35320065, 0.00000000),
                    new Point3D(50.00000000, 13.38462666, 0.00000000),
                    new Point3D(18.00000000, 13.38462666, 0.00000000)
                }
            };
            #endregion

            #region LoadPolygonInLCS
            LoadPolygonInLCS = new List<List<Point3D>>()
            {
                new List<Point3D>()
                {
                    new Point3D(0.00000000, 2.35320065, 0.00000000),
                    new Point3D(50.00000000, 2.35320065, 0.00000000),
                    new Point3D(47.11365029, 8.55682515, 0.00000000),
                    new Point3D(40.00000000, 5.00000000, 0.00000000),
                    new Point3D(33.39081339, 9.40612441, 0.00000000),
                    new Point3D(16.92313329, 13.38462666, 0.00000000)
                }
            };
            #endregion
            #region Panel1AreaAResults
            Panel1AreaAResulst = new List<List<Point3D>>()
            {
                new List<Point3D>()
                {
                    new Point3D(24.14814566, 6.47047613, 22.35320065),
                    new Point3D(29.75735090, 8.71885699, 22.35320065),
                    new Point3D(29.75735090, 8.71885699, 26.26432957)
                }
            };
            #endregion

            #region Kernels
            List<List<Point3D>>.Enumerator ePointsInGCS = ProjectionPlanePointsInGCS.GetEnumerator();
            List<List<List<Point3D>>>.Enumerator ePanelPoints = PanelPointsInGCS.GetEnumerator();
            List<List<Point3D>>.Enumerator eLoadPoints = UserLoadPointsInGCS.GetEnumerator();
            Kernels = new List<WallWindLoadKernel>();

            while (ePointsInGCS.MoveNext() && ePanelPoints.MoveNext() && eLoadPoints.MoveNext())
            {
                List<Point3D> PrPlanePoints = ePointsInGCS.Current;
                List<List<Point3D>> PanelPoints = ePanelPoints.Current;
                List<Point3D> UserLoadPoints = eLoadPoints.Current;

                WallWindLoadKernel kernel = new WallWindLoadKernel();
                kernel.La = 6;
                kernel.Lb = 12;
                kernel.SetProjectionPlane(PrPlanePoints[0], PrPlanePoints[1]);
                kernel.AddPanelPaths(PanelPoints);
                kernel.AddUserLoadPoints(UserLoadPoints);
                Kernels.Add(kernel);
            }
            
            #endregion
        }

        [TestMethod]
        public void TestTransformationOfProjectionMatrix()
        {
            List<WallWindLoadKernel>.Enumerator eKernels = Kernels.GetEnumerator();
            List<List<Point3D>>.Enumerator ePointsInLCS = ProjectionPlanePointsInLCS.GetEnumerator();
            List<List<Point3D>>.Enumerator ePointsInGCS = ProjectionPlanePointsInGCS.GetEnumerator();
            while (eKernels.MoveNext() && ePointsInLCS.MoveNext())
            {
                WallWindLoadKernel kernel = eKernels.Current;
                List<Point3D> LCSPoints = ePointsInLCS.Current; //wynik

                List<Point3D> result = kernel.GetProjectionPlanePointsInLCS();
                for (int i = 0; i < 2; i++)
                {
                    Assert.IsTrue(ZShared.Kernel.Helpers.FuzzyCompare(result[i].X, LCSPoints[i].X));
                    Assert.IsTrue(ZShared.Kernel.Helpers.FuzzyCompare(result[i].Y, LCSPoints[i].Y));
                    Assert.IsTrue(ZShared.Kernel.Helpers.FuzzyCompare(result[i].Z, LCSPoints[i].Z));
                }
            }
        }

        [TestMethod]
        public void TestTransformationOfPanelPoints()
        {

            List<WallWindLoadKernel>.Enumerator eKernels = Kernels.GetEnumerator();
            List<List<List<Point3D>>>.Enumerator ePointsInLCS = PanelPointsInLCS.GetEnumerator(); // wynik

            while (ePointsInLCS.MoveNext() && eKernels.MoveNext())
            {
                WallWindLoadKernel kernel = eKernels.Current;
                List<List<Point3D>> PanelsInLCS = ePointsInLCS.Current; //wynik

                List<List<Point3D>> ResultPanelsInLCS = kernel.GetPanelPointsInLCS();
                List<List<Point3D>>.Enumerator eResPanels = ResultPanelsInLCS.GetEnumerator();
                List<List<Point3D>>.Enumerator eTestPanels = PanelsInLCS.GetEnumerator();

                Assert.AreEqual(ResultPanelsInLCS.Count, PanelsInLCS.Count);

                while (eResPanels.MoveNext() && eTestPanels.MoveNext())
                {
                    List<Point3D> firstPanel = eResPanels.Current;
                    List<Point3D> secondPanel = eTestPanels.Current;

                    Assert.AreEqual(firstPanel.Count, secondPanel.Count);

                    for (int i = 0; i < firstPanel.Count; i++)
                    {
                        Assert.IsTrue(ZShared.Kernel.Helpers.FuzzyCompare(firstPanel[i].X, secondPanel[i].X));
                        Assert.IsTrue(ZShared.Kernel.Helpers.FuzzyCompare(firstPanel[i].Y, secondPanel[i].Y));
                        Assert.IsTrue(ZShared.Kernel.Helpers.FuzzyCompare(firstPanel[i].Z, secondPanel[i].Z));
                    }

                }
            }
        }

        [TestMethod]
        public void TestTransformedLoadPoints()
        {
            List<WallWindLoadKernel>.Enumerator eKernels = Kernels.GetEnumerator();
            List<List<Point3D>>.Enumerator eLoadPoints = UserLoadPointsInLCS.GetEnumerator();
            while (eKernels.MoveNext() && eLoadPoints.MoveNext())
            {
                WallWindLoadKernel Kernel = eKernels.Current;
                List<Point3D> LoadPointsInLCS = eLoadPoints.Current; //results
                List<Point3D> LoadPointsFromKernel = Kernel.GetUserLoadPointsInLCS();

                Assert.AreEqual(LoadPointsFromKernel.Count, LoadPointsInLCS.Count);

                for (int i = 0; i < LoadPointsInLCS.Count; i++)
                {
                    Assert.IsTrue(ZShared.Kernel.Helpers.FuzzyCompare(LoadPointsInLCS[i].X, LoadPointsFromKernel[i].X));
                    Assert.IsTrue(ZShared.Kernel.Helpers.FuzzyCompare(LoadPointsInLCS[i].Y, LoadPointsFromKernel[i].Y));
                    Assert.IsTrue(ZShared.Kernel.Helpers.FuzzyCompare(LoadPointsInLCS[i].Z, LoadPointsFromKernel[i].Z));
                }
            }
        }

        [TestMethod]
        public void TestProjectedLoadPointsHasNoElevation()
        {
            foreach (WallWindLoadKernel kernel in Kernels)
            {
                foreach (Point3D p in  kernel.GetProjectedLoadPointsInLCS())
                {
                    Assert.IsTrue(ZShared.Kernel.Helpers.FuzzyCompare(p.Z, 0));
                }
            }
        }

        [TestMethod]
        public void TestProjectedLoadPointsInLCS()
        {
            var eKernels = Kernels.GetEnumerator();
            var eUserProjectedLoadPointsInLCS = UserProjectedLoadPointsInLCS.GetEnumerator();
            while (eKernels.MoveNext() && eUserProjectedLoadPointsInLCS.MoveNext())
            {
                var kernel = eKernels.Current;
                var testResults = eUserProjectedLoadPointsInLCS.Current;
                var myResutls = kernel.GetProjectedLoadPointsInLCS();

                Assert.AreEqual(testResults.Count, myResutls.Count);

                for (int i = 0; i < testResults.Count; i++)
                {
                    Assert.IsTrue(h.FuzzyCompare(testResults[i], myResutls[i]));
                }

            }
        }

        [TestMethod]
        public void TestProjectedPanelOutline()
        {
            for (int i = 0; i < Kernels.Count; i++)
            {
                WallWindLoadKernel kernel = Kernels[i];
                List<List<Point3D>> PanelsOutlineRes = PanelsOutlineInLCS[i];
                List<List<Point3D>> results = kernel.GetPanelsOutlineInLCS();

                Assert.AreEqual(results.Count, PanelsOutlineRes.Count);

                for (int j = 0; j < PanelsOutlineRes.Count; j++)
                {
                    Assert.AreEqual(PanelsOutlineRes[j].Count, results[j].Count);
                    List<Point3D> pres = PanelsOutlineRes[j];
                    List<Point3D> res = results[j];
                    Assert.IsTrue(pres.ComparePolygons(res, 10000, 0.0002));
                }
            }
        }

        [TestMethod]
        public void CheckIfKernelsCountIsMoreThan0()
        {
            Assert.IsTrue(Kernels.Count > 0);
        }

        [TestMethod]
        public void TestLoadPolygon()
        {
            for (int i = 0; i < Kernels.Count; i++)
            {
                WallWindLoadKernel kernel = Kernels[i];
                List<Point3D> ResultPoints = LoadPolygonInLCS[i];

                List<List<Point3D>> PolygonsInLCS = kernel.GetLoadPolygon();
                Assert.AreEqual(1, PolygonsInLCS.Count);

                List<Point3D> PolygonInLCS = PolygonsInLCS[0];
                PolygonInLCS.ComparePolygons(ResultPoints, 1000000, 0.0001);
            }
        }

        [TestMethod]
        public void TestALoadPolygon()
        {
            for (int i = 0; i < Kernels.Count; i++)
            {
                WallWindLoadKernel kernel = Kernels[i];
                List<Point3D> results = ResultsInLCSA[i];

                List<Point3D> pts = kernel.GetLoadAreaPolygon(Area.A);
                Assert.IsTrue(pts.ComparePolygons(results));
            }
        }

        [TestMethod]
        public void TestBLoadPolygon()
        {
            for (int i = 0; i < Kernels.Count; i++)
            {
                WallWindLoadKernel kernel = Kernels[i];
                List<Point3D> results = ResultsInLCSB[i];

                List<Point3D> pts = kernel.GetLoadAreaPolygon(Area.B);
                Assert.IsTrue(pts.ComparePolygons(results));
            }
        }

        [TestMethod]
        public void TestCLoadPolygon()
        {
            for (int i = 0; i < Kernels.Count; i++)
            {
                WallWindLoadKernel kernel = Kernels[i];
                List<Point3D> results = ResultsInLCSC[i];

                List<Point3D> pts = kernel.GetLoadAreaPolygon(Area.C);
                Assert.IsTrue(pts.ComparePolygons(results));
            }
        }

        //w tym momencie zbudowana jest matryca na którą przykładamy obciążenia
        [TestMethod]
        public void TestGetLoadAreaPolygonInGCS()
        {
            for (int i = 0; i < Kernels.Count; i++)
            {
                WallWindLoadKernel kernel = Kernels[i];
                List<Point3D> Panel = PanelPointsInGCS[i][0];
                List<Point3D> myResults = kernel.GetLoadAreaPolygonInGCS(Area.A, Panel);
                Assert.IsTrue(Panel1AreaAResulst[i].ComparePolygons(myResults));
            }
        }
    }
}
